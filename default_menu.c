/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void new_draw_default ( void );
void hyper_menu ( void );
void constants_menu ( void );
void func_menu ( void );
void pref_menu ( void );
void change_menu ( struct menu *m );
void change_menu_and_store ( struct menu *m );

void default_draw ( void ) {
// actually, this clears the menu area. Other menu-drawing functions call this.
    DISPBOX region;
    region.left = 0;
    region.right = 127;
    region.top = 57;
    region.bottom = 63;
    Bdisp_AreaClr_VRAM ( &region );
}

void no_op ( void ) {
    ;
}

struct menu default_menu = {
    &new_draw_default,
    &hyper_menu,
    &constants_menu,
    &func_menu,
    &pref_menu,
    &no_op,
    &no_op,
    NULL,
    NULL,
    NULL
};

void new_draw_default ( void ) {
    default_draw ();//clears menu bar
    Bdisp_DrawLineVRAM ( 2, 63, 2, 57 );
    Bdisp_DrawLineVRAM ( 3, 57, 20, 57 );
    PrintMini ( 4, 59, (unsigned char*)"HYPR", MINI_OR);
    
    Bdisp_DrawLineVRAM ( 23, 63, 23, 57 );
    Bdisp_DrawLineVRAM ( 24, 57, 41, 57 );
    PrintMini ( 25, 59, (unsigned char*)"CONS", MINI_OR);

    Bdisp_DrawLineVRAM ( 44, 63, 44, 57 );
    Bdisp_DrawLineVRAM ( 45, 57, 62, 57 );
    PrintMini ( 46, 59, (unsigned char*)"FUNC", MINI_OR);

    Bdisp_DrawLineVRAM ( 65, 63, 65, 57 );
    Bdisp_DrawLineVRAM ( 66, 57, 83, 57 );
    PrintMini ( 67, 59, (unsigned char*)"PREF", MINI_OR);
}

void hyper_menu ( void ) {
    change_menu_and_store ( &hyperbolic_menu );
}

void constants_menu ( void ) {
    change_menu_and_store ( &constant_menu1 );
}

void func_menu ( void ) {
    change_menu_and_store ( &function_menu );
}

void pref_menu ( void ) {
    change_menu_and_store ( &prefix_menu1 );
}

void change_menu ( struct menu *m ) {
    if ( m != NULL ) {
	current_menu = m;
	(*(*current_menu->draw_menu))();
    }
}

void change_menu_and_store ( struct menu *m ) {
    if ( m != NULL ) {
	if ( current_menu != m ) m->exit_menu = current_menu;
	current_menu = m;
	(*(*current_menu->draw_menu))();
    }
}
