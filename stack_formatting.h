#if !defined STACKFORMAT
#define STACKFORMAT

#define DOXY ( (xy_tag ( s_f[line] ) == 1) || ( (xy_tag ( s_f[line] ) == 0) && (xydisplay==1)))
#define DODEGS ( (arg_tag ( s_f[line] ) == 1) || ( (arg_tag ( s_f[line] ) == 0) && (radians==0)))
#define DOXYNEW ( (xy_tag ( flag ) == 1) || ( (xy_tag ( flag ) == 0) && (xydisplay==1)))
#define DODEGSNEW ( (arg_tag ( flag ) == 1) || ( (arg_tag ( flag ) == 0) && (radians==0)))

void do_stack_char ( int line );
void do_memory_char ( int memory );
void do_char ( double real, double imag, unsigned short flag, char *result );
void do_all_stack_char ( void );
void old_print_stack ();
void justify_one_string ( char *string, int lr, char *result );
void justify_two_strings( char *string1, char *string2, int lr1, int lr2, char *result );
void right_justify( char *string, char *result );
void left_justify( char *string, char *result );
void new_print_stack ( int display_base, int current_line );

#endif
