/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_stack_edit ( void );
void do_pick ( void );
void do_clr_down ( void );
void do_clr_up ( void );
void do_rot_down ( void );
void do_rot_up ( void );
void do_dupn ( void );


struct menu stack_edit_menu = {
    &draw_stack_edit,
    &do_pick,
    &do_clr_down,
    &do_clr_up,
    &do_rot_down,
    &do_rot_up,
    &do_dupn,
    NULL,
    NULL,
    NULL,
};

void draw_stack_edit ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 3, 58, (unsigned char*)"PICK", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 24, 58, (unsigned char*)"CLR\xE6\x93", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 45, 58, (unsigned char*)"CLR\xE6\x92", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 66, 58, (unsigned char*)"ROT\xE6\x93", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 87, 58, (unsigned char*)"ROT\xE6\x92", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 108, 58, (unsigned char*)"DUPn", MINI_REV);
}


void do_pick ( void ) {
    s_p0--;
    s_r[s_p0 % STACKLEN] = s_r[s_p % STACKLEN];
    s_i[s_p0 % STACKLEN] = s_i[s_p % STACKLEN];
    s_f[s_p0 % STACKLEN] = s_f[s_p % STACKLEN];
    strcpy ( stack_char[s_p0 % STACKLEN], stack_char[s_p % STACKLEN] );
    if ( current_line < STACKLEN - 1 ) {
	current_line++;	
	if ( current_line - display_base == 5 ) {
	    display_base++;
	}
    }
    else {
	s_p--;
    }
    print_stack();
}

void do_clr_down ( void ) {
    int i;
    for ( i = s_p-1; (i % STACKLEN) != ( (s_p0 - 1) % STACKLEN ); i-- ) {
	s_r[i%STACKLEN] = 0.; s_i[i%STACKLEN] = 0.; s_f[i%STACKLEN] = 0;
	do_stack_char ( (i+STACKLEN-s_p)%STACKLEN );
    }
    print_stack();
}

void do_clr_up ( void ) {
    int i;
    for ( i = s_p+1; (i % STACKLEN) != ( s_p0 % STACKLEN ); i++ ) {
	s_r[i%STACKLEN] = 0.; s_i[i%STACKLEN] = 0.; s_f[i%STACKLEN] = 0;
	do_stack_char ( (i+STACKLEN-s_p)%STACKLEN );
    }
    print_stack();
}

void do_rot_up ( void ) {
    double a = s_r[s_p % STACKLEN], b = s_i[s_p % STACKLEN];
    short int f = s_f[s_p % STACKLEN], i;
    char temp[22];
    strcpy ( temp, stack_char[s_p % STACKLEN] );

    for ( i = s_p; (i % STACKLEN) != ( s_p0 % STACKLEN ); i-- ) {
	s_r[i%STACKLEN] = s_r[(i-1)%STACKLEN];
	s_i[i%STACKLEN] = s_i[(i-1)%STACKLEN];
	s_f[i%STACKLEN] = s_f[(i-1)%STACKLEN];
	strcpy ( stack_char[ i % STACKLEN], stack_char[ (i-1) % STACKLEN] );
    }
    s_r[s_p0%STACKLEN] = a; s_i[s_p0%STACKLEN] = b; s_f[s_p0%STACKLEN] = f;
    strcpy ( stack_char[ s_p0 % STACKLEN], temp );
    print_stack();
}

void do_rot_down ( void ) {
    double a = s_r[s_p0 % STACKLEN], b = s_i[s_p0 % STACKLEN];
    short int f = s_f[s_p0 % STACKLEN], i;
    char temp[22];
    strcpy ( temp, stack_char[s_p0 % STACKLEN] );

    for ( i = s_p0; (i % STACKLEN) != ( s_p % STACKLEN ); i++ ) {
	s_r[i%STACKLEN] = s_r[(i+1)%STACKLEN];
	s_i[i%STACKLEN] = s_i[(i+1)%STACKLEN];
	s_f[i%STACKLEN] = s_f[(i+1)%STACKLEN];
	strcpy ( stack_char[ i % STACKLEN], stack_char[ (i+1) % STACKLEN] );
    }
    s_r[s_p%STACKLEN] = a; s_i[s_p%STACKLEN] = b; s_f[s_p%STACKLEN] = f;
    strcpy ( stack_char[ s_p % STACKLEN], temp );
    print_stack();
}

void do_dupn ( void ) {
    int i;
    for ( i=0; i<=current_line; i++ ) {
	s_r[(s_p0-i-1)%STACKLEN] = s_r[(s_p-i)%STACKLEN];
	s_i[(s_p0-i-1)%STACKLEN] = s_i[(s_p-i)%STACKLEN];
	s_f[(s_p0-i-1)%STACKLEN] = s_f[(s_p-i)%STACKLEN];
	strcpy ( stack_char[ (s_p0-i-1) % STACKLEN], stack_char[ (s_p-i) % STACKLEN] );
    }
    s_p -= current_line+1;
    s_p0 -= current_line+1;
    print_stack();
}

/*

  do_pick

  do_clr_down

  do_clr_up

  do_rot_up

  do_rot_down

  do_dupn

  I need to be able to insert an entry. Numeric entry off the stack base overwrites; it
  does not lift the stack. FIXED; to overwrite an entry, now do DEL followed by the new entry.


  In a general stack menu, I'd want:

  rot_down, rot_up; rot_down4, rot_up4; dup2, dup3.

  Anything else? Six seems like a magic number! No - I need FILL. This could replace dup3.
  OVER is fun - it copies 2 to 1.

  I'd like a proper solution to LAST MENU and EXIT and NEXT. Basically, each menu 
  has potentially two functions - onto another menu, and back to the previous menu. It's
  natural to have EXIT to go back. However, this isn't needed for menus like CONS.

  How about this? EXIT goes back when a menu is likely to be called briefly.

  How about using parentheses to go back and forth between a list of menus? Nice idea!
  They're in the correct part of the keyboard. Then EXIT could be what it says - an exit.

  So: I need to extend the menu struct yet again. As well as six pointers-to-function, it
  needs three pointers-to-menus: one for EXIT, one for (, and one for ). These should
  probably all be initialize to NULL, and the keys for EXIT, (, and ) should test for NULL
  before trying to switch the menu. There needs to be code to initialize these menu pointers;
  or does there? I had problems getting a pointer to point to its own struct, but with this
  NULL trick perhaps I don't need to do that. 

  I'm hoping for a loop of ordinary function menus - hyperbolic, trig, x^2 etc.,
  polar-rectangular conversions, statistical functions. The need for stack menus remains.

  And memories!

  And a little character in the input line to say which stack line one is on. DONE
*/
