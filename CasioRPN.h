#if !defined CASIORPN
#define CASIORPN

#include "math.h"
#include "stdio.h"

#include <string.h>
#include <stdlib.h>

#include "arithmetic.h"
#include "formatting_routines.h"
#include "number_input.h"
#include "stack_formatting.h"
#include "stack_routines.h"
#include "tag_functions.h"
#include "trig_functions.h"
#include "menus.h"
#include "stack_edit.h"
#include "default_menu.h"
#include "memories.h"

#include "fxlib.h"
#include "keybios.h"
#include "dispbios.h"

#define STACKLEN 12
#define PI 3.14159265358979

/************************************
** Global Variables
************************************/
extern int sf_desired;
extern int sf_min;
extern int min_exponent;
extern int max_exponent;
extern char spaces[22];

extern double s_r[STACKLEN], s_i[STACKLEN], ar[12], ai[12], mr[6], mi[6];

extern unsigned short s_f[STACKLEN], af[12], mf[6];

extern char stack_char[STACKLEN][22], memory_char[6][22];
extern int s_p;
extern int s_p0, display_base, current_line;
extern double last_xr, last_xi;
extern unsigned short last_xf;


//reference stack as (pointer +/- n) % STACKLEN; I'm assuming that 0 <= (n % STACKLEN) <= 3
//Note: this doesn't work if the first argument to % is negative. Logic after the
//main loop case statement ensures that this doesn't happen.

extern char initial_input[22];
extern char whole_input[22];
extern char *input_line, *position;

extern short int number_input, exponent, decimal, exp_count, stack_edit, shift_pressed;
extern short int expnotpi;
extern short int radians;
extern short int xydisplay;

extern unsigned char imag_sign[3];
extern unsigned char angle_sign[3];

extern struct menu *current_menu;

#endif
