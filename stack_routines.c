/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

#include "stack_routines.h"

//No EXTERN variables, so the ordinary header is fine.

/**********************STACK ROUTINES*************************/


void stack_routines ( unsigned int *keypress ) {

    double a = 0, b = 0;
    unsigned short f = 0;

    switch ( *keypress ) {
	case KEY_CTRL_RIGHT://swap
	    finish_input();
	    a = s_r[s_p % STACKLEN]; 
	    b = s_i[s_p % STACKLEN]; 
	    f = s_f[s_p % STACKLEN]; 
	    s_r[s_p % STACKLEN] = s_r[(s_p+1) % STACKLEN];
	    s_i[s_p % STACKLEN] = s_i[(s_p+1) % STACKLEN];
	    s_f[s_p % STACKLEN] = s_f[(s_p+1) % STACKLEN];
	    s_r[(s_p+1) % STACKLEN] = a;
	    s_i[(s_p+1) % STACKLEN] = b;
	    s_f[(s_p+1) % STACKLEN] = f;
	    do_stack_char(0);
	    do_stack_char(1);
	    print_stack();
	    break;
	    
	case KEY_CHAR_ANS://recall last_x
	    finish_input();
	    push(last_xr, last_xi, last_xf);
	    print_stack();
	    break;
    }
}
/*
  This fills the "blank" stack entry at the top after something is removed from the stack.
  It should not be used for operations (e.g., sin (x)) which remove their arguments and
  then replace them. It should only be used when more arguments are removed then returned
  (e.g., addition, 2*real -> complex)
*/
/*
  void fill_top ( void ) { //replaced in stack_edit.c
  s_r[(s_p-1) % STACKLEN] = s_r[(s_p-2) % STACKLEN];
  s_i[(s_p-1) % STACKLEN] = s_i[(s_p-2) % STACKLEN];
  s_f[(s_p-1) % STACKLEN] = s_f[(s_p-2) % STACKLEN];
  strcpy ( stack_char[(s_p-1) % STACKLEN], stack_char[(s_p-2) % STACKLEN] );
  }
*/

void old_pop ( int n ) {
    int i;
    last_xr = s_r[ s_p % STACKLEN]; //store last x - now integrated in pop.
    last_xi = s_i[ s_p % STACKLEN];
    last_xf = s_f[ s_p % STACKLEN];
    for ( i = 0; i < n; i++ ) {
	ar[i] = s_r[ (s_p+i) % STACKLEN ];
	ai[i] = s_i[ (s_p+i) % STACKLEN ];
	af[i] = s_f[(s_p+i) % STACKLEN];
    }
    s_p += n;
}

void old_push ( double a, double b, unsigned short c ) {
    s_p--;
    if  ( b != 0. && fabs (a/b) > 1e15 ) b = 0;
    if  ( a != 0. && fabs (b/a) > 1e15 ) a = 0;
    s_r[ s_p % STACKLEN] = a;
    s_i[ s_p % STACKLEN] = b;
    s_f[ s_p % STACKLEN] = c;
    do_stack_char(0);
}

//Initialize stacks to zero
void clear_stack ( void ) {
    int i;
    s_p = 2*STACKLEN;
    for( i = 0; i < STACKLEN; i++) {
	s_r[i]=0.;
	s_i[i]=0.;
	s_f[i]=0;
	strcpy( stack_char[i], spaces); //21 spaces
	do_stack_char( i );
    }
}

void push_a ( double real, double imag, int i ) {//puts a number onto the a-stack
    ar[i] = real;
    ai[i] = imag;
    af[i] = 0;
}

void push_af ( double real, double imag, int flags, int i ) {//puts a number onto the a-stack, plus flags
    ar[i] = real;
    ai[i] = imag;
    af[i] = flags;
}
