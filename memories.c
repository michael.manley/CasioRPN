/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

/*
  clear screen;
  print the contents of the memories, plus the start of line 7;
  look to see which key invoked (if any?), print the appropriate message, and set
  a variable called current_action;
  display the menu;
  loop, waiting for a key - don't use the main loop;
  allowed key presses - 1,2,3,4,5,6: do whatever the current action is.
  Having done it, clear screen; print_stack(); init_new_number(); redraw the current menu.
  menu key - change the current action and print the new message;
  any other key, including EXIT:
  clear screen; print_stack(); init_new_number(); redraw the current menu.

  So it's not a "real" menu - current_menu doesn't change! The effects of the menu
  keys come from the key-reading loop. The "actions" require me to format numbers as strings
  so I'll have to make do_char_string more widely accessible. Otherwise it's easy!

*/

#define MEM_STO 1
#define MEM_RCL 2
#define MEM_PLUS 3
#define MEM_MINUS 4
#define MEM_EXCH 5
#define MEM_CLR 6

int current_action;
void start_memory_mode ( int key );
void draw_memory_menu ( void );
void key_loop ( void );
void print_action_message ( void );
void do_current_action ( int i );

void start_memory_mode ( int key ) {
    int i;
    if ( key == KEY_CTRL_VARS ) {
	current_action = MEM_RCL;
    }
    else {
	current_action = MEM_STO;
    }
//Clear display
    Bdisp_AllClr_DDVRAM();
//Print memories
    for ( i = 0; i < 6; i++ ) {
	locate ( 1, i+1 );
	Print ( (unsigned char*)memory_char[i] );
    }

    locate ( 1, 7 );
    PrintRev ( (unsigned char*)"Press 1-6 to " );

    draw_memory_menu ();
    print_action_message();

    key_loop ();
//Clear display
    Bdisp_AllClr_DDVRAM();
    print_stack();
    init_new_number();
    (*(*current_menu->draw_menu))();
}

void draw_memory_menu ( void ) {
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 6, 58, (unsigned char*)"STO", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 27, 58, (unsigned char*)"RCL", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 49, 58, (unsigned char*)"M+", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 70, 58, (unsigned char*)"M-", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 87, 58, (unsigned char*)"x<>M", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 108, 58, (unsigned char*)"MCLR", MINI_REV);
}

void key_loop ( void ) {
    unsigned int kp;
    int end=0;
    while ( end == 0 ) {
	GetKey ( &kp );
	switch ( kp ) {
	    case KEY_CHAR_1:
	    case KEY_CHAR_2:
	    case KEY_CHAR_3:
	    case KEY_CHAR_4:
	    case KEY_CHAR_5:
	    case KEY_CHAR_6:
		do_current_action ( kp - KEY_CHAR_1 );
		end=1;
		break;
	    case KEY_CTRL_F1:
		current_action = MEM_STO;
		print_action_message ();
		break;
	    case KEY_CTRL_F2:
		current_action = MEM_RCL;
		print_action_message ();
		break;
	    case KEY_CTRL_F3:
		current_action = MEM_PLUS;
		print_action_message ();
		break;
	    case KEY_CTRL_F4:
		current_action = MEM_MINUS;
		print_action_message ();
		break;
	    case KEY_CTRL_F5:
		current_action = MEM_EXCH;
		print_action_message ();
		break;
	    case KEY_CTRL_F6:
		current_action = MEM_CLR;
		print_action_message ();
		break;
	    default:
		end = 1;
	}
    }
}

void print_action_message ( void ) {
    locate ( 14, 7 );
    switch ( current_action ) {
	case MEM_STO:
	    PrintRev ( (unsigned char*)"store   " );
	    break;
	case MEM_RCL:
	    PrintRev ( (unsigned char*)"recall  " );
	    break;
	case MEM_PLUS:
	    PrintRev ( (unsigned char*)"add to M" );
	    break;
	case MEM_MINUS:
	    PrintRev ( (unsigned char*)"subtract" );
	    break;
	case MEM_EXCH:
	    PrintRev ( (unsigned char*)"swap x,M" );
	    break;
	case MEM_CLR:
	    PrintRev ( (unsigned char*)"clear M " );
	    break;
    }
}

void do_current_action ( int i ) {
    switch ( current_action ) {
	case MEM_STO:
	    mr[i] = s_r[s_p%STACKLEN];
	    mi[i] = s_i[s_p%STACKLEN];
	    mf[i] = s_f[s_p%STACKLEN];
	    strcpy ( memory_char[i], stack_char[s_p%STACKLEN] );
	    break;
	case MEM_RCL:
	    push ( mr[i], mi[i], mf[i] );
	    break;
	case MEM_PLUS:
	case MEM_MINUS:
	    ar[0] = s_r[s_p%STACKLEN];
	    ai[0] = s_i[s_p%STACKLEN];
	    af[0] = s_f[s_p%STACKLEN];
	    ar[1] = mr[i];
	    ai[1] = mi[i];
	    af[1] = mf[i];
	    if ( current_action == MEM_PLUS ) {
		do_plus ( 0, 1, 2, 3 );
	    }
	    else {
		do_minus ( 0, 1, 2, 3 );
	    }
	    mr[i] = ar[2];
	    mi[i] = ai[2];
	    mf[i] = af[2];
	    do_memory_char ( i );
	    break;
	case MEM_EXCH:
	    ar[1] = mr[i];
	    ai[1] = mi[i];
	    af[1] = mf[i];
	    mr[i] = s_r[s_p%STACKLEN];
	    mi[i] = s_i[s_p%STACKLEN];
	    mf[i] = s_f[s_p%STACKLEN];
	    pop ( 1 );
	    push ( ar[1], ai[1], af[1] );
	    do_memory_char ( i );
	    break;
	case MEM_CLR:
	    mr[i] = 0;
	    mi[i] = 0;
	    mf[i] = 0;
	    do_memory_char ( i );
	    break;
    }
}

void clear_memories ( void ) {
    int i;
    for ( i = 0; i < 6; i++ ) {
	mr[i] = 0.;
	mi[i] = 0.;
	mf[i] = 0;
	do_memory_char ( i );
    }
}
