/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"
#include "powers_logs.h"

void powers_logs_routines ( unsigned int *keypress ) {
    double r, theta;
    finish_input();

    switch ( *keypress ) {
	case KEY_CHAR_SQUARE: // square
	    pop (1);
	    ar[1] = ar[0]*ar[0] - ai[0]*ai[0];
	    ai[1] = 2*ar[0]*ai[0];
	    af[1] = af[0];
	    clear_real_angle ( af+1 );
	    clear_imag_angle ( af+1 );
	    push ( ar[1], ai[1], af[1] );
	    break;
	case KEY_CHAR_RECIP: // 1/x
	    pop (1);
	    af[1] = af[0];
	    clear_real_angle ( af+1 );
	    clear_imag_angle ( af+1 );
	    r = ar[0]*ar[0]+ai[0]*ai[0];
	    push ( ar[0]/r, -ai[0]/r, af[1] );
	    break;
	case KEY_CHAR_EXPN: // e^x
	    pop (1);
	    do_exp ( 0, 1 );
	    push ( ar[1], ai[1], af[1] );
	    break;
	case KEY_CHAR_EXPN10: // 10^x
	    pop (1);
	    ar[0] *= log(10.);
	    ai[0] *= log(10.);
	    do_exp ( 0, 1 );
	    push ( ar[1], ai[1], af[1] );
	    break;
	case KEY_CHAR_LN: //ln(x)
	    pop (1);
	    do_ln ( 0, 1 );
	    push ( ar[1], ai[1], af[1] );
	    break;
	case KEY_CHAR_LOG: //log(x)
	    pop (1);
	    do_ln ( 0, 1 );
	    push ( ar[1]/log(10.), ai[1]/log(10.), af[1] );
	    break;
	case KEY_CHAR_POW: // y^x = e^(x*ln(y))
	    pop (2);
	    do_pow ( 0, 1, 2, 3);
	    push ( ar[2], ai[2], af[2] );
	    fill_top();
	    break;
	case KEY_CHAR_POWROOT: // y^1/x = e^(ln(y)/x)
	    pop (2);
	    push_a ( 1., 0., 2 );
	    do_div ( 0, 2, 0, 3 );
	    do_pow ( 0, 1, 2, 3);
	    push ( ar[2], ai[2], af[2] );
	    fill_top();
	    break;
	case KEY_CHAR_ROOT: // square root
	    pop (1);
	    do_ln ( 0, 0 );
	    ar[0] /= 2;
	    ai[0] /= 2;
	    do_exp ( 0, 0 );
	    push ( ar[0], ai[0], af[0] );
	    break;
	case KEY_CHAR_CUBEROOT: // cube root
	    pop (1);
	    do_ln ( 0, 0 );
	    ar[0] /= 3;
	    ai[0] /= 3;
	    do_exp ( 0, 0 );
	    push ( ar[0], ai[0], af[0] );
	    break;
    }
    print_stack();
}

void do_exp ( int i, int j ) {// aj = e^ai; j can be i
    double r, theta;
    r = exp (ar[i]);
    theta = ai[i];
    if ( (imag_tag(af[i]) == 0) && (radians == 0) ) theta *= PI/180.;
    ar[j] = r*cos(theta);
    ai[j] = r*sin(theta);
    if ( i != j ) af[j] = af[i];
    clear_real_angle ( af+j );
    clear_imag_angle ( af+j );
}

void do_ln ( int i , int j ) {
    double r, theta;
    r = sqrt ( ar[i]*ar[i] + ai[i]*ai[i] );
    theta = atan2 ( ai[i], ar[i] );
    ar[j] = log(r);
    ai[j] = theta;
    if ( i != j ) af[j] = af[i];
    clear_real_angle ( af+j );
    if ( (arg_tag(af[i]) == 1) || ( (arg_tag(af[i]) == 0) && (radians == 0) ) ) {
	set_imag_deg ( af+j );
    }
    else {
	set_imag_rad ( af+j );
    }
}

void do_pow ( int i, int j, int k, int scr ) {//k = j^i = e^(i ln j)
    do_ln ( j, scr ); //puts ln(j) in ar[scr], ai[scr];
    do_times ( i, scr, k, scr+1 ); // any tags in scr are taken care of by do_times
    do_exp ( k, k );
}
