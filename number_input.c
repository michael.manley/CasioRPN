/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

#include "number_input.h"

void number_input_func ( unsigned int *keypress ) {

    switch ( *keypress ) {
	case KEY_CHAR_1:
	case KEY_CHAR_2:
	case KEY_CHAR_3:
	case KEY_CHAR_4:
	case KEY_CHAR_5:
	case KEY_CHAR_6:
	case KEY_CHAR_7:
	case KEY_CHAR_8:
	case KEY_CHAR_9:
	case KEY_CHAR_0:
	    if ( !exponent ) {
		add_to_string ( (char)(*keypress) );
	    }
	    else {
		add_to_exponent ( (char)*(keypress) );
	    }
	    break;
	case KEY_CHAR_DP:
	    if ( !( decimal || exponent) ) {
		decimal = 1;
		add_to_string ('.');
	    }
	    break;
	case KEY_CHAR_EXP:
	    if ( !(number_input) && !(expnotpi) ) {
		push ( (double)3.1415926535897932, 0., 0); 
		print_stack();
		init_new_number();
		break;
	    }
	    if ( !(exponent) ) {
		exponent = 1;
		if ( !(number_input) ) {
		    add_to_string ('1');
		}
		add_to_string (0x0f);
		add_to_string ('+');
		add_to_string ('0');
		add_to_string ('0');
	    }
	    break;
	case KEY_CHAR_PMINUS://change sign
	    if ( number_input ) {
		change_sign();
	    }
	    else {
		s_r[ s_p % STACKLEN ] = -s_r[ s_p % STACKLEN ];
		s_i[ s_p % STACKLEN ] = -s_i[ s_p % STACKLEN ];
		do_stack_char(0);
		print_stack();
	    }
	    break;
	case KEY_CTRL_DEL://delete
	    if ( !number_input ) { //delete lowest stack entry
		if ( !stack_edit ) {
		    s_p++;
		    fill_top();
		}
		else {
		    pop ( 1 );
		}
		print_stack ();
		break;
	    }
	    if ( exponent ) {
		position-=4;
		strcpy (position, "_    "); //5 spaces, to get rid of the old cursor
		exponent = 0;
	    }
	    else {
		if ( position > input_line+1 ) {
		    *position = ' ';
		    position--;
		    if ( *position == '.' ) {
			decimal = 0;
		    }
		    *position='_';
		}
		else {
		    init_new_number();
		}
	    }
	    print_input();
	    break;
	case KEY_CTRL_AC://all clear
	    if ( number_input ) {
		init_new_number();
		print_input();
	    }
	    else {
		PopUpWin(2);
		locate ( 4, 3 );
		Print ( (unsigned char*)"Press AC again" );
		locate ( 4, 4 );
		Print ( (unsigned char*)"to clear stack" );
		GetKey ( keypress );
		if ( *keypress == KEY_CTRL_AC ) {
		    clear_stack();
		}
		Bdisp_AllClr_DDVRAM();
		print_stack();
		init_new_number();
		(*(*current_menu->draw_menu))();
	    }
	    break;
	case KEY_CTRL_EXE://enter on the computer
	    if ( number_input ) {
		finish_input();
		do_stack_char(0);
	    }
	    else {
		if ( !stack_edit ) {
		    push ( s_r[s_p%STACKLEN], s_i[s_p%STACKLEN], s_f[s_p%STACKLEN] );
		}
		else { // want to push [s_p] into the *old* stack
		    exit_edit_mode (); // swaps s_p and s_p0
		    push ( s_r[s_p0%STACKLEN], s_i[s_p0%STACKLEN], s_f[s_p0%STACKLEN] );
		}
	    }
	    print_stack();
	    break;
    }
}

/**********************INPUT ROUTINES*************************/

void init_new_number() {
    strcpy( whole_input, initial_input );
    input_line = whole_input+1;
    position = input_line;
    number_input = 0;
    exponent = 0;
    decimal = 0;
    exp_count = 0;
    print_input();
}

void add_to_string( char character ) {
    if ( position - whole_input < 19 ) {
	number_input = 1;
	*position = character;
	position++;
	*position = '_';
	print_input();
    }
}

void add_to_exponent( char character ) {
    position--;
    *(position-1) = *position;
    add_to_string ( character );
}

void change_sign() {
    int i;
    if ( !exponent ) {
	if ( whole_input == input_line ) {
	    input_line++;
	}
	else {
	    input_line--;
	}
    }
    else {
	if ( *(position-3) == '-' ) {
	    *(position-3) = '+';
	}
	else {
	    *(position-3) = '-';
	}
    }
    print_input();
}

void finish_input ( void ) {
    char *ptr;
    if ( number_input ) {
//	if ( !stack_edit ) s_p--;
	ptr = strchr ( input_line, (char)0x0f );
	if ( ptr ) *ptr = 'e'; // stops strtod choking on the small "E"
//	s_r[ s_p % STACKLEN ] = strtod( input_line, NULL );
//	s_i[ s_p % STACKLEN ] = 0.;
//	s_f[s_p % STACKLEN] = 0;
	push ( strtod( input_line, NULL ), 0., 0 );
//	do_stack_char ( 0 );
	init_new_number();
    }
}

void print_input ( void ) { //prints the input line
    locate (1,7);
    PrintRev( (unsigned char*)input_line );
    mode_display();
}

void mode_display ( void ) { //print degrees or radians
    locate (22,7);
    if ( radians ) {
	PrintRev ( (unsigned char*)"\xac" );
    }
    else {
	PrintRev ( (unsigned char*)"\x9c" );
    }
    if ( xydisplay ) {
	PrintMini ( 112, 50, (unsigned char*)"xy", MINI_REV );
    }
    else {
	PrintMini ( 112, 50, (unsigned char*)"r\xE6\x47", MINI_REV );
    }
}
