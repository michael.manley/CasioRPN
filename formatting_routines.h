#if !defined FORMATTING
#define FORMATTING

void format_number( char *result, double number, int chars_available, int sf_desired );
void format_normal( char *result, double number, int chars_available, int sf_desired );
void format_sf(char *result, double number, int chars_available, int sf_desired);

#endif
