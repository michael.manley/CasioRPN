/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

#include "stack_formatting.h"

void do_stack_char( int line ) {
    line = (line + s_p) % STACKLEN;
    do_char ( s_r[line], s_i[line], s_f[line], stack_char[line] );
}

void do_memory_char ( int memory ) {
    do_char ( mr[memory], mi[memory], mf[memory], memory_char[memory] );
}

void do_char ( double real, double imag, unsigned short flag, char *result ) {

    char real_char[22], imag_char[22];
    char *ptr;
    double x, y, r, theta;
    
    if ( imag == 0. ) { // real number: need to check for angle tag
/*
  Is there a tag? If not, just do what I've always done.
  If there is, I need to format the number one space short
  and add the degree or whatever sign in at the end.
*/
	if ( real_tag ( flag ) == 0 ) {
	    format_number( real_char, real, 22, sf_desired );
	}
	else {
	    if 	( real_tag ( flag ) == 1 ) {
		format_number( real_char, real*180./PI, 21, sf_desired );
		ptr = real_char + strlen ( real_char );
		*ptr = (char)0x9c;
	    }
	    else {
		format_number( real_char, real, 21, sf_desired );
		ptr = real_char + strlen ( real_char );
		*ptr = (char)0xac;
	    }
	    ptr++;
	    *ptr = '\0';
	}
	justify_one_string( real_char, 1, result );
    }
    else { // complex number; this is hard!
	if ( DOXYNEW ) { //x, y mode
	    if ( real_tag ( flag ) == 0 ) {
		format_number( real_char, real, 10, sf_desired );
	    }
	    else {
		if ( real_tag ( flag ) == 1 ) {
		    format_number( real_char, real*180./PI, 9, sf_desired );
		    ptr = real_char + strlen ( real_char );
		    *ptr = (char)0x9c;
		}
		else {
		    format_number( real_char, real, 9, sf_desired );
		    ptr = real_char + strlen ( real_char );
		    *ptr = (char)0xac;
		}
		ptr++;
		*ptr = '\0';
	    }
	    if ( imag_tag ( flag ) == 0 ) {
		format_number( imag_char, imag, 10, sf_desired );
	    }
	    else {
		if ( imag_tag ( flag ) == 1 ) {
		    format_number( imag_char, imag*180./PI, 9, sf_desired );
		    ptr = imag_char + strlen ( imag_char );
		    *ptr = (char)0x9c;
		}
		else {
		    format_number( imag_char, imag, 9, sf_desired );
		    ptr = imag_char + strlen ( imag_char );
		    *ptr = (char)0xac;
		}
		ptr++;
		*ptr = '\0';
	    }
	}
	else {
	    x = real;
	    y = imag;
	    r = modulus( x, y );
	    if ( x==0. && y==0. ) {
	
		theta = 0;
	    }
	    else {
		theta = atan2(y,x);
	    }
	    if ( DODEGSNEW ) {
		theta *= 180./PI;
	    }
	    format_number( real_char, r, 10, sf_desired );
	    format_number( imag_char, theta, 9, sf_desired );
	    ptr = imag_char + strlen ( imag_char );
	    if ( DODEGSNEW ) {
		*ptr = (char)0x9c;
	    }
	    else {
		*ptr = (char)0xac;
	    }
	    ptr++;
	    *ptr = '\0';
	}
	justify_two_strings( real_char, imag_char, 1, 1, result );
    }
    while ( ptr = strchr ( result, 'e' ) ) {
	*ptr = 0x0f;
    }
}


void do_all_stack_char ( void ) {
    int i;
    for ( i = 0; i < STACKLEN; i++ ) {
	do_stack_char ( i );
    }
}

void old_print_stack( void ) {
    int i, line;
    for (i = (STACKLEN<5) ? (STACKLEN) : 5; i>=0; i--) {

	locate (1, 6-i);
	line = (s_p + i) % STACKLEN;
	Print( (unsigned char*)stack_char[ line ] );

	if ( s_i[ line ] != 0 ) {//complex; needs i or <angle>
	    locate (11, 6-i);

	    if ( DOXY ) { //x, y mode
		Print( imag_sign );
	    }
	    else {
		Print( angle_sign );
	    }
	}
    }
}

void left_justify( char *string, char *result ) {// pads out string with spaces, 
    // putting the result in result.
    // This routine assumes that string is no longer than result!
    while( *string ) {
	*result = *string;
	result++;
	string++;
    }
    while( *result ) {
	*result = ' ';
	result++;
    }
}

void right_justify( char *string, char *result ) {//pads out string with spaces on the left, leaving the result in result
    //This routine assumes that string is no longer than result!
    char *ptr1, *ptr2;
    ptr1 = string;
    ptr2 = result;
    
    while( *string ) string++;
    while( *result ) result++;
    
    string--; result--;
    while ( string >= ptr1 ) {
	*result = *string;
	string--;
	result--;
    }
    while ( result >= ptr2 ) {
	*result = ' ';
	result--;
    }
}

void justify_two_strings( char *string1, char *string2, int lr1, int lr2, char *result ) {
    //This complicated function takes two strings <= 10 characters and justifies each
    //left or right in the 10 character substring at the start and end of the string called
    //result. Result is 21 characters long (plus null).
    //For use with complex results.
    *(result+10)='\0';
    justify_one_string( string1, lr1, result );
    
    *(result+10) = ' ';
    
    result += 11;
    justify_one_string( string2, lr2, result );
}

void justify_one_string ( char *string, int lr, char *result ) {
    if ( lr == 1 ) {//left justify
	left_justify( string, result );
    }
    else 	right_justify( string, result );	
}

void new_print_stack ( int bottom, int inverse ) {//bottom, inverse, relative to s_p
    int i, line, truetop;
    truetop = ( bottom+6 > STACKLEN ) ? STACKLEN-1 : bottom+5;
    for (i = truetop; i>=bottom; i--) {
	locate (1, 6 - ( i - bottom ) );
	line = (s_p0 + i) % STACKLEN;
	if ( i == inverse ) {
	    PrintRev ( (unsigned char*)stack_char[ line ] );
	}
	else {
	    Print( (unsigned char*)stack_char[ line ] );
	}
	if ( s_i[ line ] != 0 ) {//complex; needs i or <angle>
	    locate (11, 6 - ( i - bottom ) );

	    if ( DOXY ) { //x, y mode
		if ( i == inverse ) {
		    PrintRev( imag_sign );
		}
		else {
		    Print( imag_sign );
		}
	    }
	    else {
		if ( i == inverse ) {
		    PrintRev( angle_sign );
		}
		else {
		    Print( angle_sign );
		}
	    }
	}
    }
}

