/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/

#include "fxlib.h"
#include "math.h"
#include "stdio.h"

#include "string.h"
#include "stdlib.h"

#include "keybios.h"
#include "dispbios.h"

#include "arithmetic.h"
#include "formatting_routines.h"
#include "menus.h"
#include "number_input.h"
#include "powers_logs.h"
#include "stack_formatting.h"
#include "stack_routines.h"
#include "tag_functions.h"
#include "trig_functions.h"
#include "default_menu.h"

#define STACKLEN 12
#define PI (double)3.141592653589793
#define FILEVERSION 1

/* General notes:
   So far as the stack is concerned, I want several bits.
   A real stack + an imaginary stack, for real and imaginary parts.
   Also a "string stack" in which the printed version of each line is stored.

   When an operation takes place, only those lines of the "string stack" that need
   updating are updated. 

   A pointer (taken mod STRLEN) marks the present
   stack position. The pointer is changed rather than
   moving the numbers around in the stack.

   The entire
   stack is reprinted whenever any part of it changes.
   This machine is fast, and the screen isn't that big.
*/

/************************************
 ** Global Variables
 ************************************/
int sf_desired = 6;
int sf_min = 4;
int min_exponent = 9;
int max_exponent = -4;

double s_r[STACKLEN], s_i[STACKLEN], ar[12], ai[12], mr[6], mi[6];
unsigned short s_f[STACKLEN], af[12], mf[6];
char stack_char[STACKLEN][22], memory_char[6][22];
int s_p = 2*STACKLEN;
int s_p0, display_base, current_line;
double last_xr = 0., last_xi = 0.;
unsigned short last_xf = 0;


//reference stack as (s_p +/- n) % STACKLEN. % only works as required if
//STACKLEN is positive; there's a statement at the end of the main loop which
//keeps this true.

char spaces[22] = "                     ";	//21 spaces.
char initial_input[22]="-                    ";	//-, plus 21 spaces: mistake? Changed to 20.
char whole_input[22];
char *input_line, *position;

//Some flags: not all interesting to uses, but some are.
short int number_input = 0, exponent, decimal, exp_count, stack_edit = 0, shift_pressed = 0;
short int expnotpi = 0;	//exp key enters 1e00 at start of number, not pi, when this is 1;
short int radians = 1;	//default angle mode is degrees when zero
short int xydisplay = 1;//complex display is x+iy when 1, polar when zero

//Two characters used in printing.
unsigned char imag_sign[3] = {0x7F,0x50, 0x00};
unsigned char angle_sign[3] = {0x7F,0x54, 0x00};

// a global variable so that other functions can change it.
struct menu *current_menu = &default_menu;

int handle;
void state_save ( void );
FONTCHARACTER filename[] = {'\\','\\','f','l','s','0','\\','r','p','n','s','t','a','t','e','.','d','a','t',0};

int AddIn_main(int isAppli, unsigned short OptionNum)
{
    unsigned short i;
    unsigned int kp;
    unsigned int *keypress = &kp;

    SetQuitHandler ( state_save );

    handle = Bfile_OpenFile ( filename, _OPENMODE_READ );
    if ( handle >= 0 ) { //file exists
	Bfile_ReadFile ( handle, &i, sizeof(i), -1 );
	if ( i != FILEVERSION ) {
	    Bfile_CloseFile ( handle );
	    Bfile_DeleteFile ( filename );
//Clear stack
	    clear_stack();
//Clear memories
	    clear_memories();
	}
	else {
	    Bfile_ReadFile ( handle, s_r, sizeof(s_r), -1 );
	    Bfile_ReadFile ( handle, s_i, sizeof(s_i), -1 );
	    Bfile_ReadFile ( handle, s_f, sizeof(s_f), -1 );
	    Bfile_ReadFile ( handle, stack_char, sizeof(stack_char), -1 );
	    Bfile_ReadFile ( handle, mr, sizeof(mr), -1 );
	    Bfile_ReadFile ( handle, mi, sizeof(mi), -1 );
	    Bfile_ReadFile ( handle, mf, sizeof(mf), -1 );
	    Bfile_ReadFile ( handle, memory_char, sizeof(memory_char), -1 );
	    Bfile_ReadFile ( handle, &s_p, sizeof(s_p), -1 );
	    Bfile_ReadFile ( handle, &expnotpi, sizeof(expnotpi), -1 );
	    Bfile_ReadFile ( handle, &radians, sizeof(radians), -1 );
	    Bfile_ReadFile ( handle, &xydisplay, sizeof(xydisplay), -1 );
	    Bfile_CloseFile ( handle );
	}
    }
    else { //file does not exist
//Clear stack
	clear_stack();
//Clear memories
	clear_memories();
    }

//Clear display
    Bdisp_AllClr_DDVRAM();

//display the stack
    print_stack();
    (*(*current_menu->draw_menu))();

    init_new_number();

    while (1) {
	GetKey ( keypress );
// keypress now points to the "number" of the key pressed (ASCII code)
	switch ( *keypress ) {
	    case 30006: //the shift button
		shift_pressed = 2;
		break;
	    case KEY_CHAR_1:
	    case KEY_CHAR_2:
	    case KEY_CHAR_3:
	    case KEY_CHAR_4:
	    case KEY_CHAR_5:
	    case KEY_CHAR_6:
	    case KEY_CHAR_7:
	    case KEY_CHAR_8:
	    case KEY_CHAR_9:
	    case KEY_CHAR_0:
	    case KEY_CHAR_DP:
	    case KEY_CHAR_EXP:
	    case KEY_CHAR_PMINUS://change sign
	    case KEY_CTRL_DEL://delete
	    case KEY_CTRL_AC://all clear
	    case KEY_CTRL_EXE://enter on the computer
		number_input_func ( keypress );
		break;

	    case KEY_CTRL_RIGHT://swap
	    case KEY_CHAR_ANS://recall last_x
		stack_routines ( keypress );
		break;

	    case KEY_CHAR_PLUS:
	    case KEY_CHAR_MINUS:
	    case KEY_CHAR_MULT:
	    case KEY_CHAR_DIV:
		arithmetic ( keypress );
		break;

	    case KEY_CTRL_FD:
	    case KEY_CHAR_D:
	    case KEY_CHAR_R:
	    case KEY_CTRL_FRACCNVRT: // swap x,y r,theta mode
	    case KEY_CHAR_IMGNRY: // i
	    case KEY_CHAR_THETA: // theta
	    case KEY_CHAR_ANGLE: // synonym for theta
		tag_functions ( keypress );
		break;

	    case KEY_CHAR_EXPN :
	    case KEY_CHAR_EXPN10 :
	    case KEY_CHAR_POW :
	    case KEY_CHAR_POWROOT :
	    case KEY_CHAR_SQUARE :
	    case KEY_CHAR_RECIP :
	    case KEY_CHAR_ROOT :
	    case KEY_CHAR_CUBEROOT :
	    case KEY_CHAR_LN :
	    case KEY_CHAR_LOG :
		powers_logs_routines ( keypress );
		break;

	    case KEY_CHAR_SIN :
	    case KEY_CHAR_COS :
	    case KEY_CHAR_TAN :
	    case KEY_CHAR_ASIN :
	    case KEY_CHAR_ACOS :
	    case KEY_CHAR_ATAN :
		trig_functions_routines ( keypress );
		break;

	    case KEY_CTRL_XTT :
		change_menu_and_store ( &mode_menu );
		break;
	    case KEY_CTRL_OPTN :
		change_menu_and_store ( &default_menu );
		break;
	    case KEY_CTRL_F1 :
		(*(*current_menu->f1_key))();
		break;
	    case KEY_CTRL_F2 :
		(*(*current_menu->f2_key))();
		break;
	    case KEY_CTRL_F3 :
		(*(*current_menu->f3_key))();
		break;
	    case KEY_CTRL_F4 :
		(*(*current_menu->f4_key))();
		break;
	    case KEY_CTRL_F5 :
		(*(*current_menu->f5_key))();
		break;
	    case KEY_CTRL_F6 :
		(*(*current_menu->f6_key))();
		break;
	    case KEY_CTRL_EXIT :
		if ( (stack_edit == 1) && (current_menu == &stack_edit_menu) ) { //special pleading!
		    exit_edit_mode ();
		    print_stack();
		}
		else {
		    change_menu ( current_menu->exit_menu );
		}
		break;
	    case KEY_CHAR_LPAR :
		change_menu ( current_menu->prev_menu );
		break;
	    case KEY_CHAR_RPAR :
		change_menu ( current_menu->next_menu );
		break;

	    case KEY_CTRL_UP :
		up_arrow ();
		break;
	    case KEY_CTRL_DOWN :
		down_arrow ();
		break;

	    case KEY_CTRL_VARS:
	    case KEY_CHAR_STORE:
		finish_input ();
		start_memory_mode (*keypress);
		break;
	}
	s_p = (s_p % STACKLEN) + STACKLEN;
	if ( shift_pressed > 0 ) shift_pressed--;
    }
    return 1;
}

void state_save ( void ) {
    unsigned short version = FILEVERSION;

    if ( stack_edit != 0 ) s_p = s_p0;
    Bfile_DeleteFile ( filename );
    Bfile_CreateFile ( filename, 1024 );
    handle = Bfile_OpenFile ( filename, _OPENMODE_WRITE );
    if ( handle < 0 ) {//file does not exist
	return; //well messed up!
    }
    Bfile_WriteFile ( handle, &version, sizeof(version) );
    Bfile_WriteFile ( handle, s_r, sizeof(s_r) );
    Bfile_WriteFile ( handle, s_i, sizeof(s_i) );
    Bfile_WriteFile ( handle, s_f, sizeof(s_f) );
    Bfile_WriteFile ( handle, stack_char, sizeof(stack_char) );
    Bfile_WriteFile ( handle, mr, sizeof(mr) );
    Bfile_WriteFile ( handle, mi, sizeof(mi) );
    Bfile_WriteFile ( handle, mf, sizeof(mf) );
    Bfile_WriteFile ( handle, memory_char, sizeof(memory_char) );
    Bfile_WriteFile ( handle, &s_p, sizeof(s_p) );
    Bfile_WriteFile ( handle, &expnotpi, sizeof(expnotpi) );
    Bfile_WriteFile ( handle, &radians, sizeof(radians) );
    Bfile_WriteFile ( handle, &xydisplay, sizeof(xydisplay) );
    Bfile_CloseFile ( handle );
}


//****************************************************************************
//**************                                              ****************
//**************                 Notice!                      ****************
//**************                                              ****************
//**************  Please do not change the following source.  ****************
//**************                                              ****************
//****************************************************************************


#pragma section _BR_Size
unsigned long BR_Size;
#pragma section


#pragma section _TOP

//****************************************************************************
//  InitializeSystem
//
//  param   :   isAppli   : 1 = Application / 0 = eActivity
//              OptionNum : Option Number (only eActivity)
//
//  retval  :   1 = No error / 0 = Error
//
//****************************************************************************
int InitializeSystem(int isAppli, unsigned short OptionNum)
{
    return INIT_ADDIN_APPLICATION(isAppli, OptionNum);
}

#pragma section

