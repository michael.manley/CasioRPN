/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"


void format_number( char *result, double number, int chars_available, int sf_desired );
void format_normal( char *result, double number, int chars_available, int sf_desired );
void format_sf(char *result, double number, int chars_available, int sf_desired);

/***********************FORMATTING ROUTINES*****************************************

 ********************Relevant paramaters:

sf_desired: self-descriptive
sf_min: minimum number of sf; marks change to standard form from a small decimal.
min_exponent: The power of ten at and above which standard form is displayed?
max_exponent: The power of ten at and below which standard form is displayed?
chars_available: pretty obvious, really.

***************************************/


/***********format_number: this prints a number into result,
using at most chars_available chars.
It tries to provide the desired number of significant figures.
It decides whether to use standard form or floating-point (based on
min_exponent, max_exponent, and the space actually available) and
calls two other routines.
*/

void format_number( char *result, double number, int chars_available, int sf_desired ) {
    int i, true_min, true_max;

    if ( number==0. ) {
	strcpy ( result, "0" );
	return;
    }

    i = chars_available;
    if (number<0) i--;//minus sign takes one character

//Find out when switch to standard form *must* occur
    if (i < min_exponent) true_min = i; else true_min = min_exponent; 
    if (1-i > max_exponent) true_max = 1-i; else true_max = max_exponent;

    if ( (fabs(number) >= pow(10., true_min)) || fabs(number) < pow(10., true_max) ) {
	format_sf( result, number, chars_available, sf_desired);//format in standard form
    }
    else	{
	format_normal(result, number, chars_available, sf_desired);//format as an ordinary number
    }
}

/************format_sf: this prints a number into result, using at most chars_available chars.
It tries to provide the desired number of significant figures.
*/

void format_sf(char *result, double number, int chars_available, int sf_desired) {

    int excess, pos_exp, short_exp, sf_actual;
    char *ptr, *exp_ptr;

//Print the number into result, to the correct number of sf.

    sprintf( result, "%-.*e", sf_desired-1, number );

//Now we must remove any trailing zeroes.

    ptr = result + sf_desired;
    if ( result[0] == '-' ) ptr++; //points at digit before e or its sign;

    exp_ptr = ptr + 1; //points at start of exponent
    
    if (*ptr == '0') {
	while ( *ptr == '0' ) ptr--; //finds last non-zero character
	strcpy( ptr + 1, exp_ptr);
    }
    sf_actual = sf_desired - (exp_ptr-ptr-1); //excludes zeroes

//At this point, ptr+1 is the start of the exponent: i.e., e
//[ptr is also simply result + sf_actual + (sign)]
//Here the length of result might be greater than chars_available.
//If it is, we can:
//(a) remove an initial zero in the exponent;
//(b) remove a positive exponent sign;
//(c) print at a smaller number of sf.
//As many of these as are needed are done, in this order.

    excess = strlen(result) - chars_available;

    if (excess > 0) {	//check what is worth doing.
			//But don't do it yet!
	if ( *(ptr+3) == '0' ) { 
	    excess--;
	    short_exp = 1;
	}
	else {
	    short_exp = 0;
	} //Exponent short?

	if ( (*(ptr+2) == '+') && (excess > 0) ) {
	    pos_exp = 1;
	    excess--;
	} 
	else {
	    pos_exp = 0;
	} //Exponent positive and worth removing?

	sf_actual = sf_actual-excess;
	if (sf_actual==0) sf_actual=1; //"0" is achieveable by removing the decimal
	if (sf_actual<1) {//hopeless!
	    strncpy(result,"##########",chars_available);
	    result[chars_available]='\0';
	    return;
	}
	if (excess > 0) {	//Still too long! Reprint with fewer sf
	    sprintf( result, "%-.*e", sf_actual-1, number );
	    ptr = ptr - excess;
	    if (*ptr=='e') ptr--;
	}
	if (short_exp) strcpy( ptr + 3, ptr + 4 );
	if (pos_exp) strcpy( ptr + 2, ptr + 3 );
    }
}


void format_normal( char *result, double number, int chars_available, int sf_desired ) {

    int excess, pos_exp, short_exp, new_sf, sf_actual;
    int exponent, extra_zeroes, decimal_optional, i;
    char *ptr, *exp_ptr;
    
//Print the number into result, to the correct number of sf.
    
    sprintf( result, "%-.*e", sf_desired-1, number );
    
//Now we must remove any trailing zeroes.

    ptr = result + sf_desired;
    if ( result[0] == '-' ) ptr++; //points at digit before e or its sign;
    
    exp_ptr = ptr + 1; //points at start of exponent
    
    if (*ptr == '0') {
	while ( *ptr == '0' ) ptr--; //finds last non-zero character
	strcpy( ptr + 1, exp_ptr);
    }
    sf_actual=sf_desired-(exp_ptr-ptr-1); //excludes zeroes
/*
  All the same as format_sf so far. I have something like 1.745e+04.
  It may be too long in its intended non-standard-form. How can I tell?

  positive exponent: extra zeroes = max(exp - sf + 1 , 0)
  So length is (length of this string) - 4 (exponent stuff) + extra zeroes.
  An optional decimal point is present if exp > sf - 2.

  negative exponent: extra zereos = abs (exp).
  So length is (length of this string) - 4 + abs(exp).
  An optional leading zero is automatically present.

  To do:

  extract exponent, and sign;
  Do appropriate sum.

  Positive: if it doesn't fit, there are several options.
  If exp > sf - 2 (i.e., integer) removing decimal point only helps if excess is 1.
  If not, go to standard form.

  Negative: if it doesn't fit, remove leading zero.
  If it still doesn't fit, see what number of sf are needed.
  If it is too small, go to standard form (which always gives at least 4 in 10 characters).
  Otherwise, re-print.

  Phew!
*/

    exponent = atoi( ptr+2 );

    if ( exponent >= 0 ) {
	extra_zeroes = exponent - sf_actual + 1;
	if (extra_zeroes < 0) extra_zeroes = 0;
	
	excess = (strlen(result) - 4 + extra_zeroes) - chars_available;
	
	decimal_optional = ( (exponent > sf_actual - 2) && (excess == 1) ); //only if there's a point(!)
	if ((excess <= 0)||(decimal_optional)) {
	    ptr = result + 1;
	    if (result[0] == '-') ptr++;	//ptr points at decimal
	    for(i=sf_actual-1; (i>0 && exponent > 0); i--, exponent--, ptr++) {
		*ptr = *(ptr+1);
	    }
	    for(; exponent>0; exponent--) {
		*ptr='0';
		ptr++;
	    }
	    if (!decimal_optional) {
		*ptr = '.';
		ptr = ptr+1+i;
	    }
	    *ptr = '\0';
	    return;
	}
	else {
	    format_number(result,number,chars_available,sf_actual-excess);
	    return;
	}
    }
    if (exponent < 0) {
	extra_zeroes = abs(exponent);
	excess = (strlen(result) - 4 + extra_zeroes) - chars_available;
	if (excess <= 1) {
	    //all right! return it
	    //we need: 0 if excess < 1;
	    //a '.' always;
	    //and extra_zeroes-1 zeroes
	    //and then the digits (all desired_sf of them)
	    if (result[0] == '-') result++;
	    result[1] = result[0];
	    if (excess == 1) {
		if (extra_zeroes>1){
		    for (i=sf_actual; i>0; i--) {
			result[i+extra_zeroes-1]=result[i];
		    }
		    for (i=1; i<extra_zeroes; i++) {
			result[i]='0';
		    }
		}
		result[0]='.';
		result[extra_zeroes+sf_actual]='\0';
	    }
	    else {
		for (i=sf_actual; i>0; i--) {
		    result[i+extra_zeroes]=result[i];
		}
		for (i=2; i<extra_zeroes+1; i++) {
		    result[i]='0';
		}
		result[0]='0'; result[1]='.';
		result[extra_zeroes+sf_actual+1]='\0';
	    }
	    if (*(result-1) == '-') result--;
	    return;
	}
	excess--;
	if (sf_actual - excess < sf_min) {
	    format_sf(result,number,chars_available,sf_actual);
	    return;
	    //return standard form; won't work otherwise
	}
	else {
	    format_normal(result,number,chars_available,sf_actual-excess);
	    return;//this is harder; I have to reduce the sf
	}
    }
}
