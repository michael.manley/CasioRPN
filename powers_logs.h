#if !defined POWERSLOGS
#define POWERSLOGS

void powers_logs_routines ( unsigned int *keypress );

void do_exp ( int i, int j );
void do_ln ( int i, int j );
void do_pow ( int i, int j, int k, int scr );

#endif
