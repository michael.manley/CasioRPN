/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_hyper ( void );

void do_sinh0 ( void );
void do_cosh0 ( void );
void do_tanh0 ( void );
void do_arcsinh0 ( void );
void do_arccosh0 ( void );
void do_arctanh0 ( void );

void do_sinh ( int i, int j, int scr );
void do_cosh ( int i, int j, int scr );
void do_tanh ( int i, int j, int scr );
void do_arcsinh ( int i, int j, int scr );
void do_arccosh ( int i, int j, int scr );
void do_arctanh ( int i, int j, int scr );


struct menu hyperbolic_menu = {
    &draw_hyper,
    &do_sinh0,
    &do_cosh0,
    &do_tanh0,
    &do_arcsinh0,
    &do_arccosh0,
    &do_arctanh0,
    &default_menu,
    NULL,
    NULL
};

void draw_hyper ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 4, 58, (unsigned char*)"sinh", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 25, 58, (unsigned char*)"cosh", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 46, 58, (unsigned char*)"tanh", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 67, 58, (unsigned char*)"arcs", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 88, 58, (unsigned char*)"arcc", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 109, 58, (unsigned char*)"arct", MINI_REV);

}

void do_sinh0 ( void ) {
    finish_input ();
    pop (1);
    do_sinh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}

void do_cosh0 ( void ) {
    finish_input ();
    pop (1);
    do_cosh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}

void do_tanh0 ( void ) {
    finish_input ();
    pop (1);
    do_tanh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}

void do_arcsinh0 ( void ) {
    finish_input ();
    pop (1);
    do_arcsinh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}
void do_arccosh0 ( void ) {
    finish_input ();
    pop (1);
    do_arccosh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}
void do_arctanh0 ( void ) {
    finish_input ();
    pop (1);
    do_arctanh ( 0, 0, 1 );
    push ( ar[0], ai[0], af[0] );
    print_stack ();
}

void do_sinh ( int i, int j, int scr ) {
    push_af ( -ar[i], -ai[i], af[i], scr ); //scr contains -z; i contains z
    do_exp ( i, scr+1 );
    do_exp ( scr, scr );
    do_minus ( scr, scr+1, j, scr+2 );
    ar[j] /=2; ai[j] /=2;
}

void do_cosh ( int i, int j, int scr ) {
    push_af ( -ar[i], -ai[i], af[i], scr ); //scr contains -z; i contains z
    do_exp ( i, scr+1 );
    do_exp ( scr, scr );
    do_plus ( scr+1, scr, j, scr+2 );
    ar[j] /=2; ai[j] /=2;
}

void do_tanh ( int i, int j, int scr ) {
    push_af ( -ar[i], -ai[i], af[i], scr ); //scr contains -z; i contains z
    do_exp ( i, scr+1 ); //scr+1 contains e^z
    do_exp ( scr, scr ); //scr contains e^-z
    do_minus ( scr, scr+1, scr+2, scr+4 ); // scr+2 contains 2*sinh
    do_plus ( scr, scr+1, scr+3, scr+4 ); // scr+3 contains 2*cosh
    do_div ( scr+3, scr+2, j, scr+4 );
}

void do_arcsinh ( int i, int j, int scr ) {
    push_af ( -ai[i], ar[i], af[i], scr );
    do_asin ( scr, scr, scr+1 );
    push_a ( 0., -1., scr+1 );
    do_times ( scr+1, scr, j, scr+2 );//-i asin (iz)
}

void do_arccosh ( int i, int j, int scr ) {
    push_af ( ar[i], ai[i], af[i], scr );
// ln ( z + (z+1)^0.5(z-1)^0.5 )
    clear_real_angle ( af+scr );
    clear_imag_angle ( af+scr );
    push_af ( ar[scr]-1, ai[scr], af[scr], scr+1 );
    ar[scr] += 1; //z+1 in scr; z-1 in scr+1
    push_a ( 0.5, 0., scr+2 );	
    do_pow ( scr+2, scr, scr, scr+3 ); //sqrt(z+1) in scr
    do_pow ( scr+2, scr+1, scr+1, scr+3 ); //sqrt(z-1) in scr+1
    do_times ( scr, scr+1, scr+1, scr+2 ); //products of roots in scr+1
    push_af ( ar[i], ai[i], af[i], scr );
    clear_real_angle ( af+scr );
    clear_imag_angle ( af+scr );
    do_plus ( scr, scr+1, scr+2, scr+3 ); //want this;
    do_minus ( scr+1, scr, scr+1, scr+3 ); //want 1/this;
    if ( (ar[scr+2]*ar[scr+2] + ai[scr+2]*ai[scr+2]) < (ar[scr+1]*ar[scr+1] + ai[scr+1]*ai[scr+1]) ) {
	push_a ( 1., 0., scr );
	do_div ( scr+1, scr, scr+2, scr+3 );
    }
    do_ln ( scr+2, j );
}

void do_arctanh ( int i, int j, int scr ) {
    push_af ( -ai[i], ar[i], af[i], scr );
    do_atan ( scr, scr, scr+1 );
    push_a ( 0., -1., scr+1 );
    do_times ( scr+1, scr, j, scr+2 );//-i atan (iz)
}
