/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"
#include "trig_functions.h"

void trig_functions_routines ( unsigned int *keypress ) {
    finish_input();
    pop ( 1 );

    switch ( *keypress ) {
	case KEY_CHAR_SIN:
	    do_sin ( 0, 1, 2 );
	    break;
	case KEY_CHAR_COS:
	    do_cos ( 0, 1, 2 );
	    break;
	case KEY_CHAR_TAN:
	    ar[2] = ar[0];
	    ai[2] = ai[0];
	    af[2] = af[0];
	    do_sin ( 0, 0, 3 );
	    do_cos ( 2, 2, 3 );
	    do_div ( 2, 0, 1, 3 );
	    break;
	case KEY_CHAR_ASIN:
	    do_asin ( 0, 1, 2 );
	    break;
	case KEY_CHAR_ACOS:
	    do_acos ( 0, 1, 2 );
	    break;
	case KEY_CHAR_ATAN:
	    do_atan ( 0, 1, 2 );
	    break;
    }
    push ( ar[1], ai[1], af[1] );
    print_stack();
}    

void do_sin ( int i, int j, int scr ) {// ok for real arguments; always returns real result.
    double real, imag;
    ar[scr] = ar[i]; ai[scr] = ai[i]; af[scr] = af[i];
    if ( real_tag(af[i]) == 0 && radians == 0 ) ar[scr] *= PI/180;
    real = sin(ar[scr])*cosh(ai[scr]);
    imag = cos(ar[scr])*sinh(ai[scr]);
    if ( i != j ) af[j] = af[i];
    clear_real_angle ( af+j );
    clear_imag_angle ( af+j );
    ar[j] = real;
    ai[j] = imag;
}

void do_cos ( int i, int j, int scr ) {//ok for real arguments too.
    double real, imag;
    ar[scr] = ar[i]; ai[scr] = ai[i]; af[scr] = af[i];
    if ( real_tag(af[i]) == 0 && radians == 0 ) ar[scr] *= PI/180;
    real = cos(ar[scr])*cosh(ai[scr]);
    imag = -sin(ar[scr])*sinh(ai[scr]);
    if ( i != j ) af[j] = af[i];
    clear_real_angle ( af+j );
    clear_imag_angle ( af+j );
    ar[j] = real;
    ai[j] = imag;
}

void do_asin ( int i, int j, int scr ) {
    do_invtrig ( i, j, scr, 0 );
}
void do_acos ( int i, int j, int scr ) {
    do_invtrig ( i, j, scr, 1 );
}


void do_invtrig ( int i, int j, int scr, int flag ) {
    push_af ( ar[i], ai[i], af[i], scr );
    clear_real_angle ( af+scr );
    clear_imag_angle ( af+scr );
    if ( ( modulus ( ar[i], ai[i] ) < 1e-3 )  && ( flag == 0 ) ) {//inverse sine only
	push_a ( 2., 0., scr+1 );
	do_pow ( scr+1, scr, scr+1, scr+2 ); //z^2 in scr+1; z in scr stillb
	push_a ( 0.0750000000000000, 0., scr+2 );
	do_times ( scr+1, scr+2, scr+3, scr+4 );//3/40*z^2 in scr+3
	push_a ( 0.1666666666666667, 0., scr+2);
	do_plus ( scr+2, scr+3, scr+3, scr+4 );
	do_times ( scr+1, scr+3, scr+3, scr+4 );
	push_a ( 1, 0., scr+2 );
	do_plus ( scr+2, scr+3, scr+3, scr+4 );
	do_times ( scr, scr+3, j, scr+4 ); //done it all
	if ( radians ) {
	    set_real_rad ( af+j );
	}
	else {
	    set_real_deg ( af+j );
	}
	return;
    }
// -i ln ( iz + (1-z^2)^0.5 ) for inverse sine;
// -i ln ( z + i(1-z^2)^0.5 ) for inverse cosine;
    clear_real_angle ( af+scr );
    clear_imag_angle ( af+scr );
    push_af ( -ar[scr], -ai[scr], af[scr], scr+1 );
    do_times ( scr, scr+1, scr+1, scr+2 ); //-z^2 in scr+1
    ar[scr+1] += 1; //1-z^2
    push_a ( 0.5, 0., scr );
    do_pow ( scr, scr+1, scr+1, scr+2 );//sqrt(1-z^2) in scr+1
    if ( flag ) { //if we want inverse cosine
	push_a ( 0., 1., scr );
	do_times ( scr, scr+1, scr+1, scr+2 ); //i sqrt(1-z^2) in scr+1
	push_a ( ar[i], ai[i], scr ); //z in scr
    }
    else {
	push_a ( -ai[i], ar[i], scr ); //iz in scr
    }
    do_plus ( scr, scr+1, scr+2, scr+3 ); //want this;
    do_minus ( scr, scr+1, scr+1, scr+3 ); //want 1/this;
    if ( (ar[scr+2]*ar[scr+2] + ai[scr+2]*ai[scr+2]) < (ar[scr+1]*ar[scr+1] + ai[scr+1]*ai[scr+1]) ) {
	push_a ( 1., 0., scr );
	do_div ( scr+1, scr, scr+2, scr+3 );
    }
    do_ln ( scr+2, scr+1 );
    push_a ( 0., -1., scr );
    do_times ( scr, scr+1, j, scr+2 );
}

void do_atan ( int i, int j, int scr ) {
    push_af ( ar[i], ai[i], af[i], scr );
// i/2 ln ( (i+z) / (i-z) )
    clear_real_angle ( af+scr );
    clear_imag_angle ( af+scr );
    push_a ( 0., 1., scr+1 );
    do_plus ( scr+1, scr, scr+2, scr+3 );
    do_minus ( scr, scr+1, scr+3, scr+4 );
    do_div ( scr+3, scr+2, scr+2, scr+4 );
    do_ln ( scr+2, scr+1 );
    push_a ( 0., 0.5, scr );
    do_times ( scr, scr+1, j, scr+2 );
} 

double modulus ( double a, double b ) {
    double c;
    if ( fabs(a) < fabs(b) ) {
	c = fabs(b)*sqrt(1+a*a/(b*b));
    }
    else {
	c = fabs(a)*sqrt(1+b*b/(a*a));
    }
    return c;
}

//problem with functions using \sqrt(1+/-z^2) is that when z^2 is small,
//the rounding error of about 1e-16 is important compared to z^2.
//The value of ln (iz+\sqrt(1-z^2)) is, for small z, about iz, so
//this rounding error matters.
//Solution: (i) Check for real arguments, and use the built-in functions for these.
//(ii) If z is small, use a power-series expansion of the above logarithm instead.
//-i ln( iz + \sqrt(1-z^2) ) is z + (1/6)z^3 + (3/40)z^5 + O(z^7)
//If z is O(10^-n), and E (rounding error) is about 10^-16, then we'll do better
//with the power series for n >~3. 
//So: check for z real (do in calling routines). NOT NECESSARY for now.
//In the called routines, check for |z|<10^-3 and use the power series if it is. 
//
//Maybe there isn't a problem with cosine? If z is small, I'm calculating
// -i ln ( i + z - (i/2)z^2 + iE ).
//I'm not sure why this should not be a problem, but it seems not to be.
//I know! It's my "kill imaginary parts if they are small compared to real parts"
//feature. Here, the real part is O(1), so it cuts in. It doesn't with sine.
//
//(3/40)z^5 + (1/6)z^3 + z = z ( (3/40)(z^2)^2 + (z^2)(1/6) + 1 )
// = z ( (z^2(3/40) + 1/6)z^2 + 1 )
