#if !defined STACKEDIT
#define STACKEDIT

void up_arrow ( void );
void down_arrow ( void );
void print_stack ( void );
void fill_top ( void );
void pop ( int n );
void new_pop ( int n );
void push ( double a, double b, unsigned short c );
void new_push ( double a, double b, unsigned short c );
void exit_edit_mode ( void );

#endif
