/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_pref1 ( void );
void draw_pref2 ( void );

void prefix ( int i );
void pico ( void );
void nano ( void );
void micro ( void );
void milli ( void );
void centi ( void );
void deci ( void );
void deca ( void );
void hecto ( void );
void kilo ( void );
void mega ( void );
void giga ( void );
void tera ( void );

struct menu prefix_menu1 = {
    &draw_pref1,
    &nano,
    &micro,
    &milli,
    &kilo,
    &mega,
    &giga,
    &default_menu, //exit
    &constant_menu2, //prev
    &prefix_menu2 //next
};

struct menu prefix_menu2 = {
    &draw_pref2,
    &pico,
    &centi,
    &deci,
    &deca,
    &hecto,
    &tera,
    &default_menu, //exit
    &prefix_menu1, //prev
    &constant_menu1 //next
};

void draw_pref1 ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 9, 58, (unsigned char*)"n", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 30, 58, (unsigned char*)"\xE6\x4B", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 51, 58, (unsigned char*)"m", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 73, 58, (unsigned char*)"k", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 93, 58, (unsigned char*)"M", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 115, 58, (unsigned char*)"G", MINI_REV);
}

void draw_pref2 ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 10, 58, (unsigned char*)"f", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 31, 58, (unsigned char*)"c", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 52, 58, (unsigned char*)"d", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 73, 58, (unsigned char*)"D", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 94, 58, (unsigned char*)"h", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 115, 58, (unsigned char*)"T", MINI_REV);
}

void prefix ( int i ) {
    finish_input();
    pop ( 1 );
    if ( shift_pressed ) i = -i;
    push_a ( pow ( 10., (double)i ), 0., 1 );
    do_times ( 1, 0, 2, 3 );
    push ( ar[2], ai[2], af[2] );
    print_stack ();
}

void nano ( void ) {
    prefix ( -9 );
}

void micro ( void ) {
    prefix ( -6 );
}

void milli ( void ) {
    prefix ( -3 );
}

void kilo ( void ) {
    prefix ( 3 );
}

void mega ( void ) {
    prefix ( 6 );
}

void giga ( void ) {
    prefix ( 9 );
}

void pico ( void ) {
    prefix ( -12 );
}

void centi ( void ) {
    prefix ( -2 );
}

void deci ( void ) {
    prefix ( -1 );
}

void deca ( void ) {
    prefix ( 1 );
}

void hecto ( void ) {
    prefix ( 2 );
}

void tera ( void ) {
    prefix ( 12 );
}

