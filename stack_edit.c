/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void up_arrow ( void ) {
    if ( !stack_edit ) { // enter stack edit mode
	stack_edit = 1;
	s_p0 = s_p; //old stack pointer stored
	display_base = 0; //bottom line on display, relative to sp0
	current_line = 0; //current position of sp, relative to sp0 (saves on mods)
	change_menu_and_store ( &stack_edit_menu );
	print_stack ();
	return;
    }
    if ( current_line < STACKLEN-1 ) {
	s_p++;
	current_line++;
	if ( ( current_line - display_base == 5 ) && ( current_line < STACKLEN - 1 ) ) {
	    display_base++;
	}
	print_stack ();
    }
}

void down_arrow ( void ) {
    if ( stack_edit ) {
	if ( current_line > 0 ) {
	    s_p--;
	    current_line--;
	    if ( ( current_line == display_base ) && ( current_line > 0 ) ) {
		display_base--;
	    }
	}
	else {
	    exit_edit_mode ();
	}
	print_stack ();
    }
}

void exit_edit_mode ( void ) {
    int temp;
    temp = s_p;
    s_p = s_p0;
    s_p0 = temp; //exchanges s_p and s_p0; old value of s_p might be needed
    stack_edit = 0;
    PrintMini ( 100, 50, (unsigned char*)"  ", MINI_REV );
    change_menu ( stack_edit_menu.exit_menu );
}

void print_stack ( void ) {
    unsigned char level[3];
    if ( !stack_edit ) {
	old_print_stack ();
    }
    else {
	new_print_stack ( display_base, current_line );
	sprintf ( (char*)level, "%2i", current_line+1 );
	PrintMini ( 100, 50, level, MINI_REV );
    }
}


void fill_top ( void ) { //a replacement for the old one
    if ( !stack_edit ) s_p0 = s_p;
    s_r[(s_p0-1) % STACKLEN] = s_r[(s_p0-2) % STACKLEN];
    s_i[(s_p0-1) % STACKLEN] = s_i[(s_p0-2) % STACKLEN];
    s_f[(s_p0-1) % STACKLEN] = s_f[(s_p0-2) % STACKLEN];
    strcpy ( stack_char[(s_p0-1) % STACKLEN], stack_char[(s_p0-2) % STACKLEN] );
}

void pop ( int n ) {
    if ( !stack_edit ) {
	old_pop ( n );
    }
    else {
	new_pop ( n );
    }
}

void new_pop ( int n ) {//called if stack_edit
    int i;
    last_xr = s_r[ s_p % STACKLEN]; //store last x - now integrated in pop.
    last_xi = s_i[ s_p % STACKLEN];
    last_xf = s_f[ s_p % STACKLEN];
    for ( i = 0; i < n; i++ ) {
	if ( current_line + i < STACKLEN ) {
	    ar[i] = s_r[ (s_p+i) % STACKLEN ];
	    ai[i] = s_i[ (s_p+i) % STACKLEN ];
	    af[i] = s_f[ (s_p+i) % STACKLEN ];
	}
	else {
	    ar[i] = s_r[ (s_p0-1) % STACKLEN ];
	    ai[i] = s_i[ (s_p0-1) % STACKLEN ];
	    af[i] = s_f[ (s_p0-1) % STACKLEN ];
	}
    }
    for ( i = 0; i < STACKLEN - current_line - n; i++ ) {
	s_r[ (s_p+i) % STACKLEN ] = s_r[ (s_p+i+n) % STACKLEN ];
	s_i[ (s_p+i) % STACKLEN ] = s_i[ (s_p+i+n) % STACKLEN ];
	s_f[ (s_p+i) % STACKLEN ] = s_f[ (s_p+i+n) % STACKLEN ];
	strcpy ( stack_char[ (s_p+i) % STACKLEN ], stack_char[ (s_p+i+n) % STACKLEN ] );
    }
    for ( i = STACKLEN - current_line - n; i < STACKLEN - current_line - 1; i++ ) {
	s_r[ (s_p+i) % STACKLEN ] = s_r[ (s_p+STACKLEN-current_line-1) % STACKLEN ];
	s_i[ (s_p+i) % STACKLEN ] = s_i[ (s_p+STACKLEN-current_line-1) % STACKLEN ];
	s_f[ (s_p+i) % STACKLEN ] = s_f[ (s_p+STACKLEN-current_line-1) % STACKLEN ];
	strcpy ( stack_char[ (s_p+i) % STACKLEN ], stack_char[ (s_p+STACKLEN-current_line-1) % STACKLEN ] );
    }
}

void push ( double a, double b, unsigned short c ) {
    if ( !stack_edit ) {
	old_push ( a, b, c );
    }
    else {
	new_push ( a, b, c );
    }
}

void new_push ( double a, double b, unsigned short c ) {//called if stack_edit
    int i;
    for ( i = STACKLEN - current_line - 1; i > 0; i-- ) {
	s_r [ (s_p+i) % STACKLEN ] = s_r[ (s_p+i-1) % STACKLEN ];
	s_i [ (s_p+i) % STACKLEN ] = s_i[ (s_p+i-1) % STACKLEN ];
	s_f [ (s_p+i) % STACKLEN ] = s_f[ (s_p+i-1) % STACKLEN ];
	strcpy ( stack_char [ (s_p+i) % STACKLEN ], stack_char[ (s_p+i-1) % STACKLEN ] );
    }
    if  ( b != 0. && fabs (a/b) > 1e15 ) b = 0;
    if  ( a != 0. && fabs (b/a) > 1e15 ) a = 0;
    s_r[ s_p % STACKLEN] = a;
    s_i[ s_p % STACKLEN] = b;
    s_f[ s_p % STACKLEN] = c;
    do_stack_char(0);
}



/* There is a function - new_print_stack - in stack_formatting.c which is
   used to print the stack from a specified bottom, with a specified line highlighted.
   At some point this function will have to be called during stack editing.

   The idea: pressing up_arrow puts the calculator into stack_edit mode
   (makes stack_edit true). When this happens, several things change.
   (i) The current value of s_p is stored in s_p0.
   (ii) The stack is displayed with a reverse-video line, that is moved up and down
   by the arrow keys. The stack does not wrap: the highlight stops at the top
   and at the bottom. This line is pointed to by s_p
   (iii) Because of this, all functions operate as though the highlighted line is the
   x-register.
   (iv) fill_top needs to be adjusted so that it sees the stack top as the element below
   s_p0, not below s_p. push and pop probably also need to change: some actual copying of
   elements will be involved, rather than just updating a pointer. The machine is fast:
   for reasonable stack sizes this will be fine. There will be no need for a linked list.
   (v) There is a menu to go along with this mode, involving various stack operations.
   This menu is redisplayed when up or down arrows are pressed.
   (vi) The mode is exited by pressing EXIT while this menu is displayed, or by pressing ENTER
   (which copies the s_p line back down to s_p0).

   So, we need: a new global mode variable stack_edit.
   Functions up_arrow and down_arrow in the main loop, pointing to functions in this file.
   A way to ensure that calls to print_stack result in a proper display of the stack, with
   suitable inverse video. In other words, print_stack needs to be redefined so that it
   does what it currently does when stack_edit is false, but does something new when
   stack_edit is true. This probably means redefining it in this file.
   New versions of fill_top, push, and pop, to be used only in stack_edit mode.
   A stack_edit menu.
   A new definition for ENTER pressed out of number_input mode, so that instead of DUP it
   exits the mode.

   The problem with pop is - what do I do if the data isn't there to pop? If I'm pointing
   at the top of the stack, and I press '+'? I'd like the top element to be popped repeatedly.
   Push is easier - stuff just gets pushed off the top, which is effectively what happens
   with the usual stack.
*/

