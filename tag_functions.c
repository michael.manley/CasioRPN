/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

#include "tag_functions.h"

void tag_functions ( unsigned int *keypress ) {
    int line;
    switch ( *keypress ) {

	case KEY_CTRL_FD:
	    radians = 1 - radians;
	    do_all_stack_char();
	    print_stack();
	    mode_display();
	    break;

	case KEY_CHAR_D:
	    finish_input();
	    pop ( 1 );
	    if ( ai[0] == 0 ) {//real
		if ( real_tag (af[0]) == 0 ) {
		    set_real_deg ( af );
		    ar [0] *= PI/180.; // convert to radians!
		}
		else {
		    if ( real_tag ( af[0] ) == 1 ) {
			clear_real_angle ( af );//remove angle tag
			ar [0] /= PI/180.; // convert back to degrees!
		    }
		    else { // it's radians
			set_real_deg ( af );
		    }
		}
	    }
	    else { //here, it's a complex number
// We don't tag complex numbers as a whole, as this never makes sense.
// Real and imaginary tags individually might make sense, but never
// both. (Well, both could be radians.)
// If it's currently in polar form, we change the arg_tag.
// If it's in rectangular form, we change anything currently tagged as radians into degrees,
// and anything currently tagged as degrees into nothing.

		if ( ( xy_tag ( af[0] ) == 2 ) || ( xy_tag ( af[0] ) == 0 && xydisplay == 0 ) ) {//if it's in polar
//		    if ( ( arg_tag ( af[0] ) == 2 ) || ( arg_tag ( af[0] ) == 0 && radians == 1 ) ) {//if it's in radians
		    set_arg_deg ( af ); // set the degree tag whatever the current situation
//		    }
		}
		else { // x+iy form Note: don't tag untagged pieces. Makes no sense for complex numbers.
		    if ( real_tag ( af[0] ) == 1 ) {
			clear_real_angle ( af );//remove angle tag
			ar [0] /= PI/180.; // convert back to degrees!
		    }
		    else { // it's radians
			if ( real_tag ( af[0] ) == 2 ) {
			    set_real_deg ( af );
			}
		    }
		    if ( imag_tag ( af[0] ) == 1 ) {
			clear_imag_angle ( af );//remove angle tag
			ai [0] /= PI/180.; // convert back to degrees!
		    }
		    else { // it's radians
			if ( imag_tag ( af[0] ) == 2 ) {
			    set_imag_deg ( af );
			}
		    }
		}
	    }
	    push ( ar[0], ai[0], af[0] );
	    do_stack_char(0);
	    print_stack();
	    break;

	case KEY_CHAR_R:
	    finish_input();
	    pop ( 1 );
	    if ( ai[0] == 0 ) {//real
		if ( real_tag (af[0]) == 0 ) {
		    set_real_rad ( af );
		}
		else {
		    if ( real_tag ( af[0] ) == 2 ) {
			clear_real_angle ( af );//remove angle tag
		    }
		    else { // it's degrees
			set_real_rad ( af );
		    }
		}
	    }
	    else { //here, it's a complex number
// We don't tag complex numbers as a whole, as this never makes sense.
// See D above to see what we do do.
		if ( ( xy_tag ( af[0] ) == 2 ) || ( xy_tag ( af[0] ) == 0 && xydisplay == 0 ) ) {//if it's in polar
//		    if ( ( arg_tag ( af[0] ) == 1 ) || ( arg_tag ( af[0] ) == 0 && radians == 0 ) ) {//if it's in degrees (and polar)
		    set_arg_rad ( af ); //set tag whatever its current value
//		    }
		}
		else { // x+iy form Note: don't tag untagged pieces. Makes no sense for complex numbers.
		    if ( real_tag ( af[0] ) == 2 ) {
			clear_real_angle ( af );//remove angle tag
		    }
		    else { // it's degrees
			if ( real_tag ( af[0] ) == 1 ) {
			    set_real_rad ( af );
			}
		    }
		    if ( imag_tag ( af[0] ) == 2 ) {
			clear_imag_angle ( af );//remove angle tag
		    }
		    else { // it's degrees
			if ( imag_tag ( af[0] ) == 1 ) {
			    set_imag_rad ( af );
			}
		    }
		}



		if ( ( ( arg_tag ( af[0] ) == 1 ) || ( imag_tag ( af[0] ) == 0 && radians == 0 ) )
		     && ( ( xy_tag ( af[0] ) == 2 ) || ( xy_tag ( af[0] ) == 0 && xydisplay == 0 ) ) ) {
// "if it's in degrees and if it's displayed in polar..." 
		    set_arg_rad ( af);
		}
	    }
	    push ( ar[0], ai[0], af[0] );
	    do_stack_char(0);
	    print_stack();
	    break;

	case KEY_CTRL_FRACCNVRT: // swap x,y r,theta mode
	    xydisplay = 1 - xydisplay;
	    do_all_stack_char();
	    print_stack();
	    mode_display();
	    break;

	case KEY_CHAR_IMGNRY: // i
	    finish_input();
	    line = s_p % STACKLEN;
	    if ( s_i[ line ] == 0 ) {//real
		pop(2);
		af[2] = 0;
		put_real_tag ( af+2, real_tag (af[1]) ); 
		put_imag_tag ( af+2, real_tag (af[0]) ); 
		push( ar[1], ar[0], af[2] );
		fill_top();
	    }
	    else { //it's complex
		if ( ( xy_tag ( s_f[line] ) == 2 ) || ( xy_tag ( s_f[line] ) == 0 && xydisplay == 0 ) ) {
		    set_xy_tag( s_f+line );
		    do_stack_char(0);
		}
		else {
		    pop(1);
		    af[1] = 0;
		    put_real_tag ( af+1, real_tag (af[0]) ); 
		    push( ar[0], 0., af[1] );
		    put_real_tag ( af+1, imag_tag (af[0]) ); 
		    push( ai[0], 0., af[1] );
		}
	    }
	    print_stack();
	    break;

	case KEY_CHAR_THETA: // theta
	case KEY_CHAR_ANGLE:
	    finish_input();
	    line = s_p % STACKLEN;
	    if ( s_i[ line ] == 0 ) {//real
		pop(2);
		af[2] = 0;
		put_arg_tag ( af+2, real_tag (af[0]) ); 
		if ( (real_tag(af[0]) == 0) && (radians==0) ) ar[0] *= PI/180.; //ar[0] now the angle in radians
		push( ar[1]*cos(ar[0]), ar[1]*sin(ar[0]), af[2] );
		fill_top();
	    }
	    else { //it's complex
		if ( ( xy_tag ( s_f[line] ) == 1 ) || ( xy_tag ( s_f[line] ) == 0 && xydisplay == 1 ) ) {
		    set_polar_tag( s_f+line );
		    do_stack_char(0);
		}
		else {
		    pop(1);
		    af[1] = 0;
		    push( sqrt(ar[0]*ar[0]+ai[0]*ai[0]), 0., af[1] ); //this is r
		    af[2] = arg_tag(af[0]);
		    if ( af[2] == 0 ) {//no arg tag
			if ( radians ) {
			    put_real_tag ( af+1, 2); 
			}
			else {
			    put_real_tag ( af+1, 1);
			}
		    }
		    else { 
			put_real_tag ( af+1, af[2] );
		    }
		    push( atan2(ai[0],ar[0]), 0., af[1] ); //this is theta
		}
	    }
	    print_stack();
	    break;
    }
}


//***********************TAG FUNCTIONS**************************

void clear_xy_tag ( unsigned short *f ) {
    *f = (*f) & 0xFFFC;
}

void set_xy_tag ( unsigned short *f ) {
    *f = ( (*f) & 0xFFFC ) | 0x0001;
}

void set_polar_tag ( unsigned short *f ) {
    *f = ( (*f) & 0xFFFC ) | 0x0002;
}

//**********************************

void clear_real_angle ( unsigned short *f ) {
    *f = (*f) & 0xFFF3;
}

void set_real_deg ( unsigned short *f ) {
    *f = ( (*f) & 0xFFF3 ) | 0x0004;
}

void set_real_rad ( unsigned short *f ) {
    *f = ( (*f) & 0xFFF3 ) | 0x0008;
}


//**********************************

void clear_imag_angle ( unsigned short *f ) {
    *f = (*f) & 0xFFCF;
}

void set_imag_deg ( unsigned short *f ) {
    *f = ( (*f) & 0xFFCF ) | 0x0010;
}

void set_imag_rad ( unsigned short *f ) {
    *f = ( (*f) & 0xFFCF ) | 0x0020;
}

//**********************************

void clear_arg_angle ( unsigned short *f ) {
    *f = (*f) & 0xFF3F;
}

void set_arg_deg ( unsigned short *f ) {
    *f = ( (*f) & 0xFF3F ) | 0x0040;
}

void set_arg_rad ( unsigned short *f ) {
    *f = ( (*f) & 0xFF3F ) | 0x0080;
}

//**********************************
//Note: here, the tag values are NO LONGER multipled by 1, 4, 16, 64 respectively.

unsigned short xy_tag ( unsigned short f ) {
    return f & 0x0003;
}

unsigned short real_tag ( unsigned short f ) {
    return ( f & 0x000C ) >> 2;
}

unsigned short imag_tag ( unsigned short f ) {
    return ( f & 0x0030 ) >> 4;
}

unsigned short arg_tag ( unsigned short f ) {
    return ( f & 0x00C0 ) >> 6;
}

//**********************************
//Note: here, the tag values expected are NO LONGER the desired values x (1,4,16,64).

void put_xy_tag ( unsigned short *f, unsigned short i ) {
    *f = ((*f) & 0xFFFC) + i;
} 

void put_real_tag ( unsigned short *f, unsigned short i ) {
    *f = ((*f) & 0xFFF3) + ( i << 2 );
} 

void put_imag_tag ( unsigned short *f, unsigned short i ) {
    *f = ((*f) & 0xFFCF) + ( i << 4 );
} 

void put_arg_tag ( unsigned short *f, unsigned short i ) {
    *f = ((*f) & 0xFF3F) + ( i << 6 );;
} 

