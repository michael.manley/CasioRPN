/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_function_menu ( void );
void do_recip ( void );
void do_square ( void );
void do_squareroot ( void );
void do_cube ( void );
void do_cuberoot ( void );
void do_xthroot ( void );

struct menu function_menu = {
    &draw_function_menu,
    &do_recip,
    &do_square,
    &do_squareroot,
    &do_cube,
    &do_cuberoot,
    &do_xthroot,
    &default_menu, //exit
    NULL, //prev
    NULL //next
};

void draw_function_menu ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 7, 58, (unsigned char*)"1/x", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 29, 58, (unsigned char*)"x\xE5\xC2", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 49, 58, (unsigned char*)"\x86x", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 69, 58, (unsigned char*)"x^3", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 89, 58, (unsigned char*)"3\x86x", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 110, 58, (unsigned char*)"x\x86y", MINI_REV);
}

void do_recip ( void ) {
    unsigned int i = KEY_CHAR_RECIP;
    powers_logs_routines ( &i );
}

void do_square ( void ) {
    unsigned int i = KEY_CHAR_SQUARE;
    powers_logs_routines ( &i );
}

void do_squareroot ( void ) {
    unsigned int i = KEY_CHAR_ROOT;
    powers_logs_routines ( &i );
}

void do_cube ( void ) {
    finish_input ();
    pop ( 1 );
    push_af ( ar[0], ai[0], af[0], 1 );
    do_times ( 0, 1, 1, 2 );
    do_times ( 0, 1, 2, 3 );
    push ( ar[2], ai[2], af[2] );
    print_stack ();
}

void do_cuberoot ( void ) {
    unsigned int i = KEY_CHAR_CUBEROOT;
    powers_logs_routines ( &i );
}

void do_xthroot ( void ) {
    unsigned int i = KEY_CHAR_POWROOT;
    powers_logs_routines ( &i );
}
