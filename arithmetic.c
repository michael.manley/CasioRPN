/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/

#include "CasioRPN.h"

void arithmetic (unsigned int *keypress );


void arithmetic (unsigned int *keypress ) {
    finish_input();
    pop (2);
    switch ( *keypress ) {

	case KEY_CHAR_PLUS:
	    do_plus ( 0, 1, 2, 3 );
	    break;

	case KEY_CHAR_MINUS:
	    do_minus ( 0, 1, 2, 3 );
	    break;

	case KEY_CHAR_MULT:
	    do_times ( 0, 1, 2, 3 );
	    break;

	case KEY_CHAR_DIV:
	    do_div ( 0, 1, 2, 3 );
	    break;
    }
    push ( ar[2], ai[2], af[2] );
    fill_top();
    print_stack();
}




void do_plus_flags ( int i, int j, int k, int l, int scr ) {
//does a[i] + l*a[j]; result in a[k]
//here, k can be i or j; i, j are not changed by the operation.
    double factor;
    ar[scr] = ar[i]; ai[scr] = ai[i]; af[scr] = af[i];
    ar[scr+1] = ar[j]; ai[scr+1] = ai[j]; af[scr+1] = af[j];

    if (radians) {
	factor = 1.;
    }
    else {
	factor = PI/180.;
    }

    if ( (real_tag(af[scr]) == 0) && real_tag(af[scr+1]) != 0 ) {
	put_real_tag ( af+scr, radians+1 );//tag af[scr] with default mode
	ar[scr] *= factor;
    }
    if ( (real_tag(af[scr+1]) == 0) && real_tag(af[scr]) != 0 ) {
	put_real_tag ( af+scr+1, radians+1 );//tag af[scr+1] with default mode
	ar[scr+1] *= factor;
    }
    if ( (imag_tag(af[scr]) == 0) && imag_tag(af[scr+1]) != 0 ) {
	put_imag_tag ( af+scr, radians+1 );//tag af[scr] with default mode
	ai[scr] *= factor;
    }
    if ( (imag_tag(af[scr+1]) == 0) && imag_tag(af[scr]) != 0 ) {
	put_imag_tag ( af+scr+1, radians+1 );//tag af[scr+1] with default mode
	ai[scr+1] *= factor;
    }
    
    ar[k] = ar[scr+1] + l*ar[scr];
    ai[k] = ai[scr+1] + l*ai[scr];
    af[k] = af[scr+1]; // I've changed this to scr+1 from scr; answer now has j's tags.
}

void do_plus ( int i, int j, int k, int scr ) {
    do_plus_flags ( i, j, k, 1, scr );
}

void do_minus ( int i, int j, int k, int scr ) {
    do_plus_flags ( i, j, k, -1, scr );
}


void do_times ( int i, int j, int k, int scr ) {

    double a = ar[i], b = ai[i], c = ar[j], d = ai[j], e, f;
    unsigned short fi = af[i], fj = af[j], fk;
    put_real_tag ( af+j, 0 );
    put_imag_tag ( af+j, 0 );
    af[i] = af[j];
    put_real_tag ( af+i, 0 );
    put_imag_tag ( af+i, 0 );

//first, work out (a+ib)*c, in (j).
    ar[j] = a * c;
    if ( (a != 0) && (c != 0) ) {
	if ( real_tag ( fi ) == 0 ) {
	    put_real_tag ( af+j, real_tag ( fj ) );
	}
	else {
	    if ( real_tag ( fj ) == 0 ) {
		put_real_tag ( af+j, real_tag ( fi ) );
	    }
	}
    }

    ai[j] = b * c;
    if ( (b != 0) && (c != 0) ) {
	if ( imag_tag ( fi ) == 0 ) {
	    put_imag_tag ( af+j, real_tag ( fj ) );
	}
	else {
	    if ( real_tag ( fj ) == 0 ) {
		put_imag_tag ( af+j, imag_tag ( fi ) );
	    }
	}
    }
//Next, work out (a+ib)*id = -bd + iad, in (i)
    ar[i] = -b * d;
    if ( (b != 0) && (d != 0) ) {
	if ( imag_tag ( fi ) == 0 ) {
	    put_real_tag ( af+i, imag_tag ( fj ) );
	}
	else {
	    if ( imag_tag ( fj ) == 0 ) {
		put_real_tag ( af+i, imag_tag ( fi ) );
	    }
	}
    }

    ai[i] = a * d;
    if ( (a != 0) && (d != 0) ) {
	if ( real_tag ( fi ) == 0 ) {
	    put_imag_tag ( af+i, imag_tag ( fj ) );
	}
	else {
	    if ( imag_tag ( fj ) == 0 ) {
		put_imag_tag ( af+i, real_tag ( fi ) );
	    }
	}
    }

//Now add (i) and (j), putting the result in (k)
    do_plus ( i, j, k, scr );
    e = ar[k];
    f = ai[k];
    fk = af[k];
//Restore (i), (j), (k): this roundabout code
//allows k to be the same as i or j.
    ar[i] = a;
    ai[i] = b;
    af[i] = fi;
    ar[j] = c;
    ai[j] = d;
    af[j] = fj;
    ar[k] = e;
    ai[k] = f;
    af[k] = fk;
}

void do_div (int i, int j, int k, int scr ) {// j/i
    double r2;
    int flag = 0;
    if ( (real_tag(af[i]) != 0) || (imag_tag(af[i]) != 0) ) flag = 1; //can't divide by angle
    r2 = ar[i]*ar[i]+ai[i]*ai[i];
    ai[i] = -ai[i];
    do_times ( i, j, k, scr );
    if ( i != k ) ai[i] = -ai[i]; // restore a[i] if it isn't used for the answer;
    ar[k] /= r2;
    ai[k] /= r2;
    if ( flag ) {//can't divide by angle
	put_real_tag ( af+k, 0 ); //else, let do_times handle it
	put_imag_tag ( af+k, 0 );
    }
}
