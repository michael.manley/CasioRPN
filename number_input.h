#if !defined NUMBERINPUT
#define NUMBERINPUT

void number_input_func ( unsigned int *keypress );

void init_new_number();
void add_to_string( char character );
void add_to_exponent( char character );
void change_sign();

void finish_input ( void );
void print_input ( void );

void mode_display ( void );

#endif
