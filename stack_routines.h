#if !defined STACKROUTINES
#define STACKROUTINES

void stack_routines ( unsigned int *keypress );
//void fill_top ( void );
void old_push (double a, double b, unsigned short c);
void old_pop ( int n );
void clear_stack ( void );
void push_a ( double real, double imag, int i );
void push_af ( double real, double imag, int flags, int i );

#endif
