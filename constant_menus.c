/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_cons1 ( void );
void draw_cons2 ( void );
void const_c ( void ) ;
void const_h ( void ) ;
void const_e ( void ) ;
void const_me ( void ) ;
void const_mp ( void ) ;
void const_mn ( void ) ;
void const_g ( void ) ;
void const_G ( void ) ;
void const_Na ( void ) ;
void const_eps0 ( void ) ;
void const_R ( void ) ;
void const_kB ( void ) ;

struct menu constant_menu1 = {
    &draw_cons1,
    &const_c,
    &const_h,
    &const_e,
    &const_me,
    &const_mp,
    &const_mn,
    &default_menu, //exit
    &prefix_menu2, //prev
    &constant_menu2 //next
};

struct menu constant_menu2 = {
    &draw_cons2,
    &const_g,
    &const_G,
    &const_Na,
    &const_eps0,
    &const_R,
    &const_kB,
    &default_menu, //exit
    &constant_menu1, //prev
    &prefix_menu1 //next
};

void draw_cons1 ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 10, 58, (unsigned char*)"c", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 31, 58, (unsigned char*)"h", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 52, 58, (unsigned char*)"e", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 68, 58, (unsigned char*)"m_e", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 89, 58, (unsigned char*)"m_p", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 110, 58, (unsigned char*)"m_n", MINI_REV);
}

void draw_cons2 ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 10, 58, (unsigned char*)"g", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 31, 58, (unsigned char*)"G", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 47, 58, (unsigned char*)"N_a", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 71, 58, (unsigned char*)"e0", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 94, 58, (unsigned char*)"R", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 111, 58, (unsigned char*)"k_B", MINI_REV);
}

void const_c ( void ) {
    finish_input ();
    push ( 2.99792458e8, 0, 0 );
    print_stack ();
}
void const_h ( void ) {
    finish_input ();
    push ( 6.62606876e-34, 0, 0 );
    print_stack ();
}
void const_e ( void ) {
    finish_input ();
    push ( 1.602176462e-19, 0, 0 );
    print_stack ();
}
void const_me ( void ) {
    finish_input ();
    push ( 9.10938188e-31, 0, 0 );
    print_stack ();
}
void const_mp ( void ) {
    finish_input ();
    push ( 1.67262158e-27, 0, 0 );
    print_stack ();
}
void const_mn ( void ) {
    finish_input ();
    push ( 1.67492716e-27, 0, 0 );
    print_stack ();
}
void const_g ( void ) {
    finish_input ();
    push ( 9.80665, 0, 0 );
    print_stack ();
}
void const_G ( void ) {
    finish_input ();
    push ( 6.673e-11, 0, 0 );
    print_stack ();
}
void const_Na ( void ) {
    finish_input ();
    push ( 6.02214199e23, 0, 0 );
    print_stack ();
}
void const_eps0 ( void ) {
    finish_input ();
    push ( 8.854187817e-12, 0, 0 );
    print_stack ();
}
void const_R ( void ) {
    finish_input ();
    push ( 8.314472, 0, 0 );
    print_stack ();
}
void const_kB ( void ) {
    finish_input ();
    push ( 1.3806503e-23, 0, 0 );
    print_stack ();
}

