#if !defined MENUS
#define MENUS

void no_op ( void );
void default_draw ( void );

struct menu {
    void ( *draw_menu ) ( void ); //pointer to function
    void ( *f1_key ) ( void ); //pointer to function
    void ( *f2_key ) ( void ); //pointer to function
    void ( *f3_key ) ( void ); //pointer to function
    void ( *f4_key ) ( void ); //pointer to function
    void ( *f5_key ) ( void ); //pointer to function
    void ( *f6_key ) ( void ); //pointer to function
    struct menu *exit_menu; //pointer to menu invoked by exit key
    struct menu *prev_menu; //pointer to menu invoked by ( key
    struct menu *next_menu; //pointer to menu invoked by ) key
};

extern struct menu default_menu;
extern struct menu hyperbolic_menu;
extern struct menu constant_menu1;
extern struct menu constant_menu2;
extern struct menu mode_menu;
extern struct menu stack_edit_menu;
extern struct menu function_menu;
extern struct menu prefix_menu1;
extern struct menu prefix_menu2;


/* Space between each menu block: 2 px, and at start and end.
   Width of each menu block: 19px.
   19*6 + 2*7 = 114+14 = 128. tick!
   Starts: 2, 23, 44, 65, 86, 107
   Ends: 20, 41, 62, 83, 104, 126
*/

//What follows is the simple-minded menu-drawing routine.
//Good for testing.
//First line: black square.
//Lines 2, 3: black partial border.
//		Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
//		Bdisp_DrawLineVRAM ( 2, 63, 2, 57 );
//		Bdisp_DrawLineVRAM ( 3, 57, 20, 57 );
//              PrintMini ( 4, 58, (unsigned char*)"sinh", MINI_REV);
//
//		Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
//		Bdisp_DrawLineVRAM ( 23, 63, 23, 57 );
//		Bdisp_DrawLineVRAM ( 24, 57, 41, 57 );
//              PrintMini ( 25, 58, (unsigned char*)"sinh", MINI_REV);
//
//		Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
//		Bdisp_DrawLineVRAM ( 44, 63, 44, 57 );
//		Bdisp_DrawLineVRAM ( 45, 57, 62, 57 );
//              PrintMini ( 46, 58, (unsigned char*)"sinh", MINI_REV);
//
//		Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
//		Bdisp_DrawLineVRAM ( 65, 63, 65, 57 );
//		Bdisp_DrawLineVRAM ( 66, 57, 83, 57 );
//              PrintMini ( 67, 58, (unsigned char*)"sinh", MINI_REV);
//
//		Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
//		Bdisp_DrawLineVRAM ( 86, 63, 86, 57 );
//		Bdisp_DrawLineVRAM ( 87, 57, 104, 57 );
//              PrintMini ( 88, 58, (unsigned char*)"sinh", MINI_REV);
//
//		Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
//		Bdisp_DrawLineVRAM ( 107, 63, 107, 57 );
//		Bdisp_DrawLineVRAM ( 108, 57, 125, 57 );
//              PrintMini ( 109, 58, (unsigned char*)"sinh", MINI_REV);
//

#endif
