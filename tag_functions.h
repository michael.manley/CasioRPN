#if !defined TAGFUNCTIONS
#define TAGFUNCTIONS

void tag_functions ( unsigned int *keypress );

void clear_xy_tag ( unsigned short *f );
void set_xy_tag ( unsigned short *f );
void set_polar_tag ( unsigned short *f );
void clear_real_angle ( unsigned short *f );
void set_real_deg ( unsigned short *f );
void set_real_rad ( unsigned short *f );
void clear_imag_angle ( unsigned short *f );
void set_imag_deg ( unsigned short *f );
void set_imag_rad ( unsigned short *f );
void clear_arg_angle ( unsigned short *f );
void set_arg_deg ( unsigned short *f );
void set_arg_rad ( unsigned short *f );

unsigned short xy_tag ( unsigned short f );
unsigned short real_tag ( unsigned short f );
unsigned short imag_tag ( unsigned short f );
unsigned short arg_tag ( unsigned short f );
void put_xy_tag ( unsigned short *f, unsigned short i );
void put_real_tag ( unsigned short *f, unsigned short i );
void put_imag_tag ( unsigned short *f, unsigned short i );
void put_arg_tag ( unsigned short *f, unsigned short i );


#endif
