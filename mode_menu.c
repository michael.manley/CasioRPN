/****************************************************************/
/*								*/
/*   RPN Calculator for Casio FX-9860 Slim (etc.)		*/
/*								*/
/*								*/
/*								*/
/*   Copyright (c) N.J Dowrick 2009				*/
/*								*/
/****************************************************************/
#include "CasioRPN.h"

void draw_mode ( void );
void do_D ( void ) ;
void do_R ( void ) ;
void do_D_R ( void ) ;
void do_i ( void ) ;
void do_theta ( void ) ;
void do_xypolar ( void ) ;

struct menu mode_menu = {
    &draw_mode,
    &do_D,
    &do_R,
    &do_D_R,
    &do_i,
    &do_theta,
    &do_xypolar,
    &default_menu,
    NULL,
    NULL
};

void draw_mode ( void ) {
    default_draw ();
    Bdisp_AreaReverseVRAM ( 2, 57, 20, 63 );
    PrintMini ( 10, 58, (unsigned char*)"D", MINI_REV);
    Bdisp_AreaReverseVRAM ( 23, 57, 41, 63 );
    PrintMini ( 31, 58, (unsigned char*)"R", MINI_REV);
    Bdisp_AreaReverseVRAM ( 44, 57, 62, 63 );
    PrintMini ( 48, 58, (unsigned char*)"D-R", MINI_REV);

    Bdisp_AreaReverseVRAM ( 65, 57, 83, 63 );
    PrintMini ( 73, 58, (unsigned char*)"\x7F\x50", MINI_REV);
    Bdisp_AreaReverseVRAM ( 86, 57, 104, 63 );
    PrintMini ( 94, 58, (unsigned char*)"\x7F\x54", MINI_REV);
    Bdisp_AreaReverseVRAM ( 107, 57, 125, 63 );
    PrintMini ( 111, 58, (unsigned char*)"R-P", MINI_REV);
}

void do_D ( void ) {
    unsigned int kp = KEY_CHAR_D;
    tag_functions ( &kp );
}
void do_R ( void ) {
    unsigned int kp = KEY_CHAR_R;
    tag_functions ( &kp );
}
void do_D_R ( void ) {
    unsigned int kp = KEY_CTRL_FD;
    tag_functions ( &kp );
}
void do_i ( void ) {
    unsigned int kp = KEY_CHAR_IMGNRY;
    tag_functions ( &kp );
}
void do_theta ( void ) {
    unsigned int kp = KEY_CHAR_ANGLE;
    tag_functions ( &kp );
}
void do_xypolar ( void ) {
    unsigned int kp = KEY_CTRL_FRACCNVRT;
    tag_functions ( &kp );
}
