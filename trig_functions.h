#if !defined TRIGFUNCTIONS
#define TRIGFUNCTIONS

void trig_functions_routines ( unsigned int *keypress );

void do_sin ( int i, int j, int scr );
void do_cos ( int i, int j, int scr );
void do_asin ( int i, int j, int scr );
void do_acos ( int i, int j, int scr );
void do_atan ( int i, int j, int scr );
void do_invtrig ( int i, int j, int scr, int flag );
double modulus ( double a, double b );

#endif
